
-- @file:        dml.psql
-- @author:      sfarnsworth, <sfarnsworth@hawk.iit.edu>
-- @created:     11.20.2019
-- @updated:
--

DO
$do$
DECLARE

	first_names varchar(16)[20] := '{"John","Stephanie","Peter","Cruella","Jasmine","Gregory","Yusef","Fred","Popeye","Victoria","Jacob","Kweku","Zoya","Gabriel","Kaleb","Shaun","Ivan","Curtis","Evan","Donavan"}';		 
	last_names varchar(16)[26] := '{"A.","B.","C.","D.","E.","F.","G.","H.","I.","J.","K.","L.","M.","N.","O.","P.","Q.","R.","S.","T.","U.","V.","W.","X.","Y.","Z."}';	
	street_names varchar(22)[12] := '{"Crestood","Dominic","Cass","Lemont","Finlaw","Royce","Boughton","Michigan","Water","Market","Main","Freedom","Halsted"}'; 
	street_type varchar(5)[5] := '{"St.","Blvd.","Rd.","Way","Ln."}';  
	city_names varchar(16)[22] := '{"Sausalito","Sunbury","Custer","Ipswich","Union","Fort Coral","Herlingen","Kinsport","Roseburg","Worland","Fall River","Pottstown","Winnemucca","Ruston","Eastchester","Carmel","Elwood","Pendleton","Gallatin","Winslow","Dixon", "Princeton"}';
	state_names char(2)[50] := '{"AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"}';	     
	titles varchar(32)[2] := '{"Employee", "Manager"}';

BEGIN
	-- ADD 2 STAFF (1 manager + 1 emplyee)
	FOR i IN 1..2 LOOP
	
	    INSERT INTO Users(user_id,staff_id,customer_id,first_name,last_name,password) VALUES (DEFAULT,DEFAULT,NULL,first_names[(random()*19+1)::int],last_names[(random()*25+1)::int],md5('root'));
	    
	    INSERT INTO Staff(staff_id,street,apt_number,city,state,zip,title,salary)
	    VALUES (currval('users_staff_id_seq'),CONCAT(((random()*9999+1)::int)::text,' ',street_names[(random()*11+1)::int],' ',street_type[(random()*4+1)::int]),null,city_names[(random()*21+1)::int],state_names[(random()*48+1)::int],LPAD(((random()*99998+1)::int)::text,'5','0'),titles[i],random()*60000+40000);
	    
	END LOOP;

	
	
	INSERT INTO Users(user_id,staff_id,customer_id,first_name,last_name,password) VALUES (DEFAULT,NULL,DEFAULT,first_names[(random()*19+1)::int],last_names[(random()*25+1)::int],md5('root'));
	FOR i in 1..3 LOOP
		INSERT INTO Shipping(shipping_id,customer_id,street,apt_number,city,state,zip,active)
		VALUES (DEFAULT,currval('users_customer_id_seq'),CONCAT(((random()*9999+1)::int)::text,' ',street_names[(random()*11+1)::int],' ',street_type[(random()*4+1)::int]),' ',city_names[(random()*21+1)::int],'IL',LPAD(((random()*99998+1)::int)::text,'5','0'),DEFAULT);
	END LOOP;

	INSERT INTO Payments(customer_id,card_number,street,apt_number,city,state,zip)
	VALUES (currval('users_customer_id_seq'),CONCAT(LPAD(((random()*9998+1)::int)::text,'4','0'),LPAD(((random()*9998+1)::int)::text,'4','0'),LPAD(((random()*9998+1)::int)::text,'4','0'),LPAD(((random()*9998+1)::int)::text,'4','0')),' ',' ',' ','AA',' ');

	INSERT INTO Cart(cart_id,customer_id,shipping_id,card_number,status)
	VALUES (DEFAULT, currval('users_customer_id_seq'),currval('shipping_shipping_id_seq'),NULL,DEFAULT);



	UPDATE Payments
	SET street=s.street,apt_number=s.apt_number,city=s.city,state=s.state,zip=s.zip
	FROM Shipping s
	WHERE payments.customer_id = s.customer_id;

	UPDATE cart
	SET card_number = p.card_number
	FROM Payments p
	WHERE cart.customer_id = p.customer_id;

	-- ADD 2 SUPPLIERS
	INSERT INTO Supplier (supplier_id,name,street,city,state,zip) VALUES (DEFAULT,'Digikey',CONCAT(((random()*9999+1)::int)::text,' ',street_names[(random()*11+1)::int],' ',street_type[(random()*4+1)::int]),city_names[(random()*21+1)::int],state_names[(random()*48+1)::int],LPAD(((random()*99998+1)::int)::text,'5','0'));
	INSERT INTO Supplier (supplier_id,name,street,city,state,zip) VALUES (DEFAULT,'Mouser',CONCAT(((random()*9999+1)::int)::text,' ',street_names[(random()*11+1)::int],' ',street_type[(random()*4+1)::int]),city_names[(random()*21+1)::int],state_names[(random()*48+1)::int],LPAD(((random()*99998+1)::int)::text,'5','0'));


	-- ADD 1 WAREHOUSE
	INSERT INTO Warehouse (warehouse_id,street,city,state,zip,capacity) VALUES (DEFAULT,CONCAT(((random()*9999+1)::int)::text,' ',street_names[(random()*11+1)::int],' ',street_type[(random()*4+1)::int]),city_names[(random()*21+1)::int],'IL',LPAD(((random()*99998+1)::int)::text,'5','0'),1500.0);


	-- ADD 6 CATEGORIES with 5 PRODUCTS each
	-- ADD 5 state-dependent PRICES per product
	-- ADD 1 SUPPLIER PRICE per product
	-- ADD stock for warehouse
	INSERT INTO Category(category_id,cat) VALUES (DEFAULT,'Semiconductors');
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.008','SN74LS08');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+1);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+1);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+1);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.008','SN74LS04');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+1);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+1);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+1);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.008','SN74LS01');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+1);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+1);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+1);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.008','SN74LS00');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+1);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+1);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+1);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.008','SN74LS86');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+1);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+1);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+1);
	END LOOP;

	
	INSERT INTO Category(category_id,cat) VALUES (DEFAULT,'Electromechanical');
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.10','Servo Motor');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+19);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+19);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+19);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.12','Bushless DC Motor 400RPM');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+19);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+19);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+19);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.12','Brushless DC Motor 2500RPM');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+19);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+19);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+19);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.12','Brushless DC Motor 4000RPM');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+19);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+19);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+19);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.25','2-channel 120v 10A Relay');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+19);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+19);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+19);
	END LOOP;

	
	INSERT INTO Category(category_id,cat) VALUES (DEFAULT,'Power/Circuit Protection');
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.08','Circuit Breaker 250V 5A');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+9);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+9);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+9);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.08','Circuit Breaker 120V 5A');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+9);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+9);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+9);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.08','15A Fuse');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+9);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+9);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+9);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.08','10A Fuse');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+9);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+9);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+9);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.08','5A Fuse');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+9);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+9);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+9);
	END LOOP;
	

	INSERT INTO Category(category_id,cat) VALUES (DEFAULT,'Passives');
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.03','Resistor 220ohm');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+0.10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+0.10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+0.10);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.03','Resistor 310ohm');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+0.10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+0.10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+0.10);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.03','Resistor 1Kohm');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+0.10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+0.10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+0.10);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.03','Capacitor 0.2 nF');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+0.10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+0.10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+0.10);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.03','Capacirot 0.44 nF');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()+0.10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()+0.10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()+0.10);
	END LOOP;


	INSERT INTO Category(category_id,cat) VALUES (DEFAULT,'Cables/Wires');
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.15','10pc 3.25in Jumpers(M)');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+10);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.15','10pc 2.00in Jumpers(M)');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+10);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.15','10pc 1.00in Jumpers(M)');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+10);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.15','10pc 3.25in Jumpers(F)');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+10);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.15','10pc 2.00in Jumpers(F)');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*2+10);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*2+10);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*2+10);
	END LOOP;


	INSERT INTO Category(category_id,cat) VALUES (DEFAULT,'Developer Tools');
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.20','PIC18Fx Curiosity Nano');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*5+25);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*5+25);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*5+25);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.20','ATMEGAx Curiosity Nano');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*5+25);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*5+25);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*5+25);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.20','Arduino Nano v3.3');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*5+25);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*5+25);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*5+25);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.20','Arduino Micro');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*5+25);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*5+25);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*5+25);
	END LOOP;
	INSERT INTO Product(product_id,category_id,volume,name) VALUES (DEFAULT,currval('category_category_id_seq'),'0.20','Raspberry Pi4');
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),1,random()*5+25);
	INSERT INTO Supplies(product_id,supplier_id,supplier_price) VALUES (currval('product_product_id_seq'),2,random()*5+25);
	INSERT INTO Stocks(product_id,warehouse_id,quantity) VALUES (currval('product_product_id_seq'),1,random()*999+1);
	FOR i in 1..50 LOOP
	    INSERT INTO Prices(product_id,state,price) VALUES (currval('product_product_id_seq'),state_names[i],random()*5+25);
	END LOOP;

	FOR i in 1..3 LOOP
		INSERT INTO Contains(product_id,cart_id,quantity,bought_price)
		VALUES (i,1,random()*9+1,DEFAULT);
	END LOOP;

	GRANT CONNECT ON DATABASE storeapp TO app;
	GRANT SELECT,UPDATE,INSERT,DELETE ON ALL TABLES IN SCHEMA public TO app;
	GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO app;
END
$do$;
