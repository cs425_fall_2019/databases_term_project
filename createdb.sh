#!/bin/bash

#  @file:     createdb.sh
#  @author:   sfarnsworth, <sfarnsworth@hawk.iit.edu>
#  @created:  11.20.2019
#

createdb storeapp
psql storeapp -f ddl.sql

read -p 'Create the app user? [Do this only once] (y/n): '
if [ $REPLY = 'y' ]; then
	psql storeapp -f appuser.sql
fi

psql storeapp -f dml.sql
