
import os
import sql
import kivy
import gui_globals
from kivy.uix.widget import Widget
from kivy.graphics import *
kivy.require("1.11.1")

sidebar_size_x = 160
loginbox_size_y = 140
userbox_size_y = 80
cartbox_size_y = 110
filterbox_size_y = 35
searchbar_size_y = 35
default_line_height = 25

class separator(Widget):
    def __init__(self,h,w,r,g,b,a,**kwargs):
        super().__init__(**kwargs)

        if h != None:
            self.size_hint_y = None
            self.height = h

        if w != None:
            self.size_hint_x = None
            self.width = w

        with self.canvas.after:
            Color(r,g,b,a)
            self.rect = Rectangle(pos=self.pos, size_hint=(1,1))
            self.bind(pos=self.update_rect, size=self.update_rect)

    def update_rect(self, *args):
        self.rect.pos = self.pos
        self.rect.size = self.size

