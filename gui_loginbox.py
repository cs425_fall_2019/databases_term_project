
import os
import sql
import kivy
import gui_globals
from gui_genericbox import generic_box
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.dropdown import DropDown
from kivy.graphics import *
kivy.require("1.11.1")

sidebar_size_x = 160
loginbox_size_y = 140
userbox_size_y = 80
cartbox_size_y = 110
filterbox_size_y = 35
searchbar_size_y = 35
default_line_height = 25

class loginbox(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        
        self.rows = 5
        self.cols = 1
        self.size_hint = (None,None)
        self.size = (sidebar_size_x,loginbox_size_y)
        self.padding = 4

        self.box1 = generic_box(sidebar_size_x-8,default_line_height)
        self.box2 = generic_box(sidebar_size_x-8,default_line_height)
        self.box3 = generic_box(sidebar_size_x-8,default_line_height)
        self.box4 = generic_box(sidebar_size_x-8,default_line_height)
        self.box5 = generic_box(sidebar_size_x-8,default_line_height)

        self.add_widget(self.box1)
        self.add_widget(self.box2)
        self.add_widget(self.box3)
        self.add_widget(self.box4)
        self.add_widget(self.box5)

        self.welcome_msg = Label(text='',
                                 size_hint=(1,1),
                                 text_size=self.size,
                                 halign="center",
                                 valign="center",
                                 padding=(4,4),
                                 color=(0,0,0,1),
                                 font_size=12)
        self.ip_username = TextInput(hint_text="username",
                                     multiline=False,
                                     font_size=10)
        self.ip_passwd = TextInput(hint_text="password",
                                   password=True,
                                   allow_copy=False,
                                   multiline=False,
                                   font_size=10)

        self.login_submit = Button(text="Login",font_size=12) # box1
        self.account_info = Button(text="Account Info",font_size=12) # box3
        self.staff_portal = Button(text="Staff Portal",font_size=12) # box3 or box4
        self.logout_submit = Button(text="Logout",font_size=12) # box4 or box5
        
        self.box1.add_widget(self.ip_username)
        self.box2.add_widget(self.ip_passwd)
        self.box4.add_widget(self.login_submit)

        with self.canvas.before:
            Color(1,1,1,1)
            self.rect = Rectangle(size=self.size, pos=self.pos)
            self.bind(pos=self.update_rect, size=self.update_rect)

    def update_rect(self,*args):
        self.rect.pos = self.pos
        self.rect.size = self.size
