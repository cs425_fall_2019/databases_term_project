
-- @file:        ddl.psql
-- @author:      nmark
-- @author:      sfarnsworth, <sfarnsworth@hawk.iit.edu>
-- @author:	 pcudzich, <pcudzich@hawk.iit.edu>
-- @created:     11.13.2019
-- @updated:     11.25.2019
--


CREATE SEQUENCE users_staff_id_seq;
CREATE SEQUENCE users_customer_id_seq;

CREATE TABLE Users
(
	user_id serial PRIMARY KEY,
	staff_id integer UNIQUE DEFAULT nextval('users_staff_id_seq'),
	customer_id integer UNIQUE DEFAULT nextval('users_customer_id_seq'),
	first_name varchar(16) NOT NULL,
	last_name varchar(16) NOT NULL,
	password varchar(32) NOT NULL,
	staff_active boolean NOT NULL DEFAULT TRUE,
	customer_active boolean NOT NULL DEFAULT TRUE
);

ALTER SEQUENCE users_staff_id_seq OWNED BY Users.staff_id;
ALTER SEQUENCE users_customer_id_seq OWNED BY Users.customer_id;

CREATE TABLE Staff
(
	staff_id integer PRIMARY KEY,
	street varchar(32) NOT NULL,
	apt_number varchar(8),
	city varchar(16) NOT NULL,
	state char(2) NOT NULL,
	zip char(5) NOT NULL,
	title varchar(32) NOT NULL,
	salary numeric(10, 2) NOT NULL,
	FOREIGN KEY(staff_id) references Users(staff_id)
);

CREATE TABLE Payments
(
	customer_id integer NOT NULL,
	card_number char(16) NOT NULL,
	street varchar(32) NOT NULL,
	apt_number varchar(8),
	city varchar(16) NOT NULL,
	state char(2) NOT NULL,
	zip varchar(10) NOT NULL,
	active boolean NOT NULL DEFAULT TRUE,
	PRIMARY KEY(customer_id, card_number),
	FOREIGN KEY(customer_id) references Users(customer_id)
);

CREATE TABLE Shipping
(
	shipping_id serial PRIMARY KEY,
	customer_id integer NOT NULL,
	street varchar(32) NOT NULL,
	apt_number varchar(8),
	city varchar(16) NOT NULL,
	state char(2) NOT NULL,
	zip varchar(10) NOT NULL,
	active boolean NOT NULL DEFAULT TRUE,
	FOREIGN KEY(customer_id) references Users(customer_id)
);

CREATE TABLE Category
(
	category_id serial PRIMARY KEY,
	cat varchar(32) NOT NULL
);

INSERT INTO Category (category_id, cat) VALUES (DEFAULT,'Other');

CREATE TABLE Product
(
	product_id serial PRIMARY KEY,
	category_id integer NOT NULL DEFAULT 1,
	volume numeric(8,5) NOT NULL,
	name varchar(32) NOT NULL,
	active boolean NOT NULL DEFAULT TRUE,
	FOREIGN KEY(category_id) references Category(category_id)
);

CREATE TABLE Prices
(
	product_id integer NOT NULL,
	state char(2) NOT NULL,
	price numeric(7, 2) DEFAULT NULL,
	PRIMARY KEY(product_id, state),
	FOREIGN KEY(product_id) references Product(product_id)
);

CREATE TABLE Supplier
(
	supplier_id serial PRIMARY KEY,
	name varchar(32) NOT NULL,
	street varchar(32),
	city varchar(16),
	state varchar(16),
	zip varchar(10)
);

CREATE TABLE Supplies
(
	product_id integer NOT NULL,
	supplier_id integer NOT NULL,
	supplier_price numeric(7, 2) NOT NULL,
	PRIMARY KEY(product_id, supplier_id),
	FOREIGN KEY(product_id) references Product(product_id),
	FOREIGN KEY(supplier_id) references Supplier(supplier_id)
);

CREATE TABLE Cart
(
	cart_id serial PRIMARY KEY,
	customer_id integer NOT NULL,
	shipping_id integer NOT NULL,
	card_number char(16),
	status varchar(10) NOT NULL default 'open',
	order_date TIMESTAMP DEFAULT NULL,
	FOREIGN KEY(customer_id,card_number) references Payments(customer_id,card_number),
	FOREIGN KEY(shipping_id) references Shipping(shipping_id)
);

CREATE TABLE Contains
(
	product_id integer NOT NULL,
	cart_id integer NOT NULL,
	quantity integer NOT NULL DEFAULT 1,
	bought_price numeric(7, 2) DEFAULT NULL,
	PRIMARY KEY(product_id, cart_id),
	FOREIGN KEY(product_id) references Product(product_id),
	FOREIGN KEY(cart_id) references Cart(cart_id)
);

CREATE TABLE Warehouse
(
	warehouse_id serial PRIMARY KEY,
	street varchar(32) NOT NULL,
	city varchar(16) NOT NULL,
	state varchar(16) NOT NULL,
	zip varchar(10) NOT NULL,
	capacity integer NOT NULL
);


CREATE TABLE Stocks
(
	product_id integer NOT NULL,
	warehouse_id integer NOT NULL,
	quantity integer NOT NULL,
	PRIMARY KEY(product_id, warehouse_id),
	FOREIGN KEY(product_id) references Product(product_id),
	FOREIGN KEY(warehouse_id) references Warehouse(warehouse_id)
);

CREATE FUNCTION balance(cust_id integer)
	RETURNS numeric(7,2) AS $$
	DECLARE bal numeric(7,2);
BEGIN
	SELECT SUM(contains.bought_price) INTO bal
	FROM cart NATURAL JOIN contains
	WHERE cart.customer_id = cust_id
	AND cart.status = 'issued';
	RETURN bal;
END;
$$ LANGUAGE plpgsql;

