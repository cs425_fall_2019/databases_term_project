
import os
import sql
import kivy
import gui_globals
from gui_separator import separator
from gui_genericbox import generic_box
from kivy.uix.gridlayout import GridLayout
from kivy.graphics import *
kivy.require("1.11.1")

sidebar_size_x = 160
loginbox_size_y = 140
userbox_size_y = 80
cartbox_size_y = 110
filterbox_size_y = 35
searchbar_size_y = 35
default_line_height = 25

class sidebar(GridLayout):

    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        
        self.rows = 6
        self.cols = 1
        self.size_hint = (None,None)
        self.size = (sidebar_size_x,100)
        self.padding = 2

        self.box1 = generic_box(sidebar_size_x,loginbox_size_y)
        self.box2 = generic_box(sidebar_size_x,cartbox_size_y)
        self.box3 = generic_box(sidebar_size_x,searchbar_size_y)
        self.box4 = generic_box(sidebar_size_x,filterbox_size_y)

        self.add_widget(self.box1)
        self.add_widget(separator(4,None,0,0,0,1))
        self.add_widget(self.box2)
        self.add_widget(separator(4,None,0,0,0,1))
        self.add_widget(self.box3)
        self.add_widget(self.box4)
