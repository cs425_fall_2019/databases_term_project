import psycopg2
import gui_globals
from decimal import *

#User functions

conn = None
cur = None

def add_user(passw,f_name,l_name,cust=False,staff=False):
    #adds new user and returns id
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""INSERT INTO users (user_id,password,first_name,last_name,customer_id,staff_id) 
                    VALUES (DEFAULT,MD5(%s),%s,%s,%s,%s);""",(passw,f_name,l_name,None,None))
    gui_globals.cur.execute("SELECT user_id FROM users ORDER BY user_id DESC LIMIT 1;")
    u_id = gui_globals.cur.fetchone()[0]
    if cust:
        gui_globals.cur.execute("""UPDATE users SET customer_id = DEFAULT, customer_active = TRUE
                        WHERE user_id = %s AND customer_id IS NULL;""",(u_id,))
    if staff:
        gui_globals.cur.execute("""UPDATE users SET staff_id = DEFAULT, staff_active = TRUE
                        WHERE user_id = %s AND staff_id IS NULL;""",(u_id,))
    return u_id

def add_customer(u_id,passw=None,f_name=None,l_name=None):
    gui_globals.conn
    gui_globals.cur
    if u_id == None:
        u_id = add_user(passw,f_name,l_name,True)
    else:
        gui_globals.cur.execute("""UPDATE users SET customer_id = DEFAULT, customer_active = TRUE 
                        WHERE user_id = %s AND customer_id IS NULL;""",(u_id,))
    gui_globals.conn.commit()
    return u_id

def add_staff(u_id,passw=None,f_name=None,l_name=None,street=None,apt=None,city=None,state=None,zipcode=None,title=None,salary=None):
    gui_globals.conn
    gui_globals.cur
    if u_id == None:
        u_id = add_user(passw,f_name,l_name,staff=True)
    else:
        gui_globals.cur.execute("""UPDATE users SET staff_id = DEFAULT, staff_active = TRUE 
                        WHERE user_id = %s AND staff_id IS NULL;""",(u_id,))
    gui_globals.cur.execute("SELECT staff_id FROM users WHERE user_id = %s;", (u_id,))
    s_id = gui_globals.cur.fetchone()[0]
    gui_globals.cur.execute("""INSERT INTO staff (staff_id, street, apt_number,city, state, zip, title, salary)
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s);""",(s_id,street,apt,city,state,zipcode,title,salary))
    gui_globals.conn.commit()
    return u_id

def get_ids(u_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT customer_id, staff_id, customer_active, staff_active FROM users WHERE user_id = %s;",(u_id,))
    row = list(gui_globals.cur.fetchone())
    if not row[2]:
        row[0] = None
    if not row[3]:
        row[1] = None
    return tuple(row[0:2])

def get_name(u_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT first_name, last_name FROM users WHERE user_id = %s;",(u_id,))
    ret = gui_globals.cur.fetchone()
    if ret is None:
        return (None,None)
    return ret

def get_staff_address(s_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT street, apt_number, city, state, zip FROM staff WHERE staff_id = %s;",(s_id,))
    return gui_globals.cur.fetchone()

def get_title(s_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT title FROM staff WHERE staff_id = %s;",(s_id,))
    return gui_globals.cur.fetchone()[0]

def get_salary(s_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT salary FROM staff WHERE staff_id = %s;",(s_id,))
    return gui_globals.cur.fetchone()[0]

def modify_staff(s_id,street=None,apt=None,city=None,state=None,zipcode=None,title=None,salary=None):
    gui_globals.conn
    gui_globals.cur
    attrs = []
    vals = []
    if street != None:
        attrs.append("street = %s")
        vals.append(street)
    if apt != None:
        attrs.append("apt_number = %s")
        vals.append(apt)
    if city != None:
        attrs.append("city = %s")
        vals.append(city)
    if state != None:
        attrs.append("state = %s")
        vals.append(state)
    if zipcode != None:
        attrs.append("zip = %s")
        vals.append(zipcode)
    if title != None:
        attrs.append("title = %s")
        vals.append(title)
    if salary != None:
        attrs.append("salary = %s")
        vals.append(salary)
    vals.append(s_id)
    query = "UPDATE staff SET "
    for attr in attrs:
        query = query + attr
        if attr != attrs[-1]:
            query = query + ","
        query = query + " "
    query = query + "WHERE staff_id = %s;"
    gui_globals.cur.execute(query,tuple(vals));
    gui_globals.conn.commit()
    return

def get_user(u_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT user_id, customer_id, staff_id, first_name, last_name, customer_active, staff_active
                    FROM users WHERE user_id = %s;""",(u_id,))
    return gui_globals.cur.fetchone()

def get_balance(cust_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT balance(%s);",(cust_id,))
    return gui_globals.cur.fetchone()

def get_staff():
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT staff_id, first_name, last_name, staff_active FROM users ORDER BY last_name ASC;""")
    return gui_globals.cur.fetchall()

def activate_customer(c_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("UPDATE users SET customer_active = TRUE WHERE customer_id = %s",(c_id,))
    gui_globals.conn.commit()
    return

def deactivate_customer(c_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("UPDATE users SET customer_active = FALSE WHERE customer_id = %s",(c_id,))
    gui_globals.conn.commit()
    return

def activate_staff(s_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("UPDATE users SET staff_active = TRUE WHERE staff_id = %s",(s_id,))
    gui_globals.conn.commit()
    return

def deactivate_staff(s_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("UPDATE users SET staff_active = FALSE WHERE staff_id = %s",(s_id,))
    gui_globals.conn.commit()
    return

#payments functions

def add_payment(cust_id, c_num, street, apt, city, state, zipcode):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""INSERT INTO payments (customer_id, card_number, street, apt_number, city, state, zip) 
                                VALUES (%s,%s,%s,%s,%s,%s,%s);""",(cust_id,c_num,street,apt,city,state,zipcode))
    gui_globals.conn.commit()
    return

def get_payment(cust_id,c_num):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT card_number, street, apt_number, city, state, zip FROM payments
                    WHERE card_number = '%s' AND customer_id = %s;""",(c_num, cust_id))
    return gui_globals.cur.fetchone()

def get_payments(cust_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT card_number, street, apt_number, city, state, zip
                    FROM payments WHERE customer_id = %s AND active = TRUE;""",(cust_id,))
    return gui_globals.cur.fetchall()

def modify_payment(cust_id,c_num,new_c_num=None,street=None,apt=None,city=None,state=None,zipcode=None):
    gui_globals.conn
    gui_globals.cur
    attrs = []
    vals = []
    if street != None:
        attrs.append("street = %s")
        vals.append(street)
    if apt != None:
        attrs.append("apt_number = %s")
        vals.append(apt)
    if city != None:
        attrs.append("city = %s")
        vals.append(city)
    if state != None:
        attrs.append("state = %s")
        vals.append(state)
    if zipcode != None:
        attrs.append("zip = %s")
        vals.append(zipcode)
    if new_c_num != None:
        attrs.append("card_number = '%s'")
        vals.append(new_c_num)
    vals.append(cust_id)
    vals.append(c_num)
    query = "UPDATE payments SET "
    for attr in attrs:
        query = query + attr
        if attr != attrs[-1]:
            query = query + ","
        query = query + " "
    query = query + "WHERE customer_id = %s AND card_number = '%s';"
    gui_globals.cur.execute(query,tuple(vals));
    gui_globals.conn.commit()
    return

def del_payment(cust_id,c_num):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT cart_id FROM cart WHERE customer_id = %s AND card_number = '%s' 
                                            AND status != 'open';""",(cust_id,c_num))
    row = gui_globals.cur.fetchone()
    if row is None:
        gui_globals.cur.execute("DELETE FROM payments WHERE customer_id = %s AND card_number = '%s';",(cust_id,c_num))
    else:
        gui_globals.cur.execute("UPDATE payments SET active = FALSE WHERE customer_id = %s AND card_number = '%s';"(cust_id,c_num))
    gui_globals.conn.commit()
    return

#shipping address functions

def add_ship_addr(cust_id,street,city,state,zipcode,apt=None):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""INSERT INTO shipping (shipping_id,customer_id, street, apt_number, city, state, zip)
                                VALUES (DEFAULT,%s,%s,%s,%s,%s,%s);""",(cust_id,street,apt,city,state,zipcode))
    gui_globals.cur.execute("SELECT shipping_id FROM shipping ORDER BY shipping_id DESC LIMIT 1;")
    gui_globals.conn.commit()
    return gui_globals.cur.fetchone()[0]

def get_address(ship_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT customer_id, street, apt_number, city, state, zip FROM shipping
                    WHERE shipping_id = %s;""",(ship_id,))
    return gui_globals.cur.fetchone()

def get_addresses(cust_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT shipping_id, street, apt_number, city, state, zip
                    FROM shipping WHERE customer_id = %s AND active = TRUE;""",(cust_id,))
    return gui_globals.cur.fetchall()

def modify_ship_addr(ship_id,street=None,city=None,state=None,zipcode=None,apt=None):
    gui_globals.conn
    gui_globals.cur
    attrs = []
    vals = []
    if street != None:
        attrs.append("street = %s")
        vals.append(street)
    if apt != None:
        attrs.append("apt_number = %s")
        vals.append(apt)
    if city != None:
        attrs.append("city = %s")
        vals.append(city)
    if state != None:
        attrs.append("state = %s")
        vals.append(state)
    if zipcode != None:
        attrs.append("zip = %s")
        vals.append(zipcode)
    vals.append(ship_id)
    query = "UPDATE shipping SET "
    for attr in attrs:
        query = query + attr
        if attr != attrs[-1]:
            query = query + ","
        query = query + " "
    query = query + "WHERE shipping_id = %s;"
    gui_globals.cur.execute(query,tuple(vals));
    gui_globals.conn.commit()
    return

def del_ship_addr(ship_id,cust_id):
    gui_globals.conn
    gui_globals.cur
    if len(get_addresses(cust_id)) > 1:
        gui_globals.cur.execute("SELECT cart_id FROM cart WHERE shipping_id = %s AND status != 'open';",(ship_id,))
        row = gui_globals.cur.fetchone()
        if row is None:
            gui_globals.cur.execute("DELETE FROM shipping WHERE shipping_id = %s;",(ship_id,))
        else:
            gui_globals.cur.execute("UPDATE shipping SET active = FALSE WHERE shipping_id = %s;"(ship_id,))
        gui_globals.cur.execute("SELECT cart_id FROM cart WHERE shipping_id = %s AND status = 'open';",(ship_id,))
        cart = gui_globals.cur.fetchone()
        if cart is not None:
            gui_globals.cur.execute("""SELECT shipping_id FROM shipping WHERE customer_id = %s 
                                                            AND active = TRUE LIMIT 1;""",(cust_id,))
            sh_id = gui_globals.cur.fetchone()[0]
            gui_globals.cur.execute("UPDATE cart SET shipping_id = %s WHERE cart_id = %s;",(sh_id,cart[0]))
        gui_globals.conn.commit()
        return True
    else:
        return False

#category functions
    
def add_category(cat):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("INSERT INTO category (category_id, cat) VALUES (DEFAULT,%s);",(cat,))
    gui_globals.cur.execute("SELECT category_id FROM category ORDER BY category_id DESC LIMIT 1;")
    gui_globals.conn.commit()
    return gui_globals.cur.fetchone()[0]

def get_category(cat_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT cat FROM category WHERE category_id = %s;",(cat_id,))
    return gui_globals.cur.fetchone()[0]

def get_categories():
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT category_id, cat FROM category ORDER BY category ASC;")
    return gui_globals.cur.fetchall()

def modify_category(cat_id,cat):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("UPDATE category SET cat = %s WHERE category_id = %s;",(cat,cat_id))
    gui_globals.conn.commit()
    return

def del_category(cat_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("UPDATE product SET category_id = DEFAULT WHERE category_id = %s;",(cat_id,))
    gui_globals.cur.execute("DELETE FROM category WHERE category_id = %s;",(cat_id,))
    gui_globals.conn.commit()
    return

#product functions

def add_product(name,volume,cat_id = None):
    gui_globals.conn
    gui_globals.cur
    if cat_id is None:
        gui_globals.cur.execute("""INSERT INTO product (product_id, category_id, volume, name)
                                VALUES (DEFAULT,DEFAULT,%s,%s);""",(volume,name))
    else:
        gui_globals.cur.execute("""INSERT INTO product (product_id, category_id, volume, name)
                                VALUES (DEFAULT,%s,%s,%s);""",(cat_id,volume,name))
    gui_globals.cur.execute("SELECT product_id FROM product ORDER BY product_id DESC LIMIT 1;")
    gui_globals.conn.commit()
    return gui_globals.cur.fetchone()[0]

def get_product(prod_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT product_id, name, volume, category_id FROM product
                    WHERE product_id = %s;""",(prod_id,))
    return gui_globals.cur.fetchone()

def search_products(search=None,cat_id=None, cart_id=None):
    gui_globals.conn
    gui_globals.cur
    query = "SELECT product.product_id, name, volume, cat"
    vals = []
    if cart_id is None:
        query = query + ", NULL FROM product NATURAL JOIN category"
        if cat_id is not None:
            query = query + " WHERE category_id = %s"
            vals.append(cat_id)
    else:
        gui_globals.cur.execute("SELECT state FROM cart NATURAL JOIN shipping WHERE cart_id = %s;",(cart_id,))
        vals.append(gui_globals.cur.fetchone()[0])
        query = query + ", price FROM product NATURAL JOIN category LEFT JOIN prices ON product.product_id = prices.product_id WHERE state = %s"
        if cat_id is not None:
            query = query + " AND category_id = %s"
            vals.append(cat_id)
    if search is not None:
        if cart_id is None and cat_id is None:
            query = query + " WHERE"
        else:
            query = query + " AND"
        query = query + " name ILIKE %s"
        vals.append("%"+search+"%")
    query = query + " ORDER BY name ASC;"
    gui_globals.cur.execute(query, tuple(vals))
    return gui_globals.cur.fetchall()

def modify_product(prod_id,name=None,volume=None,cat_id=None):
    gui_globals.conn
    gui_globals.cur
    attrs = []
    vals = []
    if name != None:
        attrs.append("name = %s")
        vals.append(name)
    if volume != None:
        attrs.append("volume = %s")
        vals.append(volume)
    if cat_id != None:
        attrs.append("category_id = %s")
        vals.append(cat_id)
    vals.append(prod_id)
    query = "UPDATE product SET "
    for attr in attrs:
        query = query + attr
        if attr != attrs[-1]:
            query = query + ","
        query = query + " "
    query = query + "WHERE product_id = %s;"
    gui_globals.cur.execute(query,tuple(vals));
    gui_globals.conn.commit()
    return

def del_product(prod_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("DELETE FROM contains WHERE product_id = %s AND bought_price IS NULL",(prod_id,))
    gui_globals.cur.execute("DELETE FROM supplies WHERE product_id = %s;",(prod_id,))
    gui_globals.cur.execute("DELETE FROM stocks WHERE product_id = %s;",(prod_id,))
    gui_globals.cur.execute("DELETE FROM prices WHERE product_id = %s;",(prod_id,))
    gui_globals.cur.execute("SELECT cart_id FROM cart NATURAL JOIN contains WHERE product_id = %s AND status != 'open';",(prod_id,))
    row = gui_globals.cur.fetchone()
    if row is None:
        gui_globals.cur.execute("DELETE FROM product WHERE product_id = %s;",(prod_id,))
    else:
        gui_globals.cur.execute("UPDATE product SET active = FALSE WHERE product_id = %s;"(prod_id,))
    gui_globals.conn.commit()
    return

#price functions

def add_price(prod_id,state,price):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("INSERT INTO prices (product_id,state,price) VALUES (%s,%s,%s);",(prod_id,state,price))
    gui_globals.conn.commit()
    return

def get_price(prod_id, state):
    gui_globals.conn
    gui_globals.cur
    if state is None:
        return 0
    gui_globals.cur.execute("SELECT price FROM prices WHERE product_id = %s AND state = %s;",(prod_id,state))
    row = gui_globals.cur.fetchone()
    if row is None:
        return 0
    return row[0]

def modify_price(prod_id,state,price):
    gui_globals.conn
    gui_globals.cur
    if get_price(prod_id, state) <= 0:
        add_price(prod_id, state, price)
        return
    gui_globals.cur.execute("UPDATE prices SET price = %s WHERE product_id = %s AND state = %s;",(price,prod_id,state))
    gui_globals.conn.commit()
    return

def del_price(prod_id,state):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("UPDATE prices SET price = DEFAULT WHERE product_id = %s AND state = %s;",(prod_id,state))
    gui_globals.conn.commit()
    return

#supplier functions

def add_supplier(name,street,city,state,zipcode):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""INSERT INTO supplier (supplier_id,name,street,city,state,zip)
                                VALUES (DEFAULT,%s,%s,%s,%s,%s);""",(name,street,city,state,zipcode))
    gui_globals.cur.execute("SELECT supplier_id FROM supplier ORDER BY supplier_id DESC LIMIT 1;")
    gui_globals.conn.commit()
    return gui_globals.cur.fetchone()[0]

def get_supplier(supp_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT name, street, city, state, zip FROM supplier WHERE supplier_id =%s;",(supp_id,))
    return gui_globals.cur.fetchone()

def get_suppliers():
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT supplier_id, name, street, city, state, zip FROM supplier;")
    return gui_globals.cur.fetchall()

def modify_supplier(supp_id,name=None,street=None,city=None,state=None,zipcode=None):
    gui_globals.conn
    gui_globals.cur
    attrs = []
    vals = []
    if street != None:
        attrs.append("street = %s")
        vals.append(street)
    if name != None:
        attrs.append("name = %s")
        vals.append(name)
    if city != None:
        attrs.append("city = %s")
        vals.append(city)
    if state != None:
        attrs.append("state = %s")
        vals.append(state)
    if zipcode != None:
        attrs.append("zip = %s")
        vals.append(zipcode)
    vals.append(supp_id)
    query = "UPDATE supplier SET "
    for attr in attrs:
        query = query + attr
        if attr != attrs[-1]:
            query = query + ","
        query = query + " "
    query = query + "WHERE supplier_id = %s;"
    gui_globals.cur.execute(query,tuple(vals));
    gui_globals.conn.commit()
    return

def del_supplier(supp_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("DELETE FROM supplies WHERE supplier_id = %s;",(supp_id,))
    gui_globals.cur.execute("DELETE FROM supplier WHERE supplier_id = %s;",(supp_id,))
    gui_globals.conn.commit()
    return

#supply functions
   
def add_supply(prod_id,supp_id,price):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""INSERT INTO supplies (product_id, supplier_id, supplier_price)
                                VALUES (%s,%s,%s);""",(prod_id,supp_id,price))
    gui_globals.conn.commit()
    return

def get_supp_prices(prod_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT supplier_id, name, supplier_price FROM supplies NATURAL JOIN supplier
                    WHERE product_id = %s;""",(prod_id,))
    return gui_globals.cur.fetchall()

def modify_supply(prod_id,supp_id,price):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""UPDATE supplies SET supplier_price = %s 
                        WHERE product_id = %s AND supplier_id = %s;""",(price,prod_id,supp_id))
    gui_globals.conn.commit()
    return

def del_supply(prod_id,supp_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("DELETE FROM supplies WHERE product_id = %s AND supplier_id = %s;",(prod_id,supp_id))
    gui_globals.conn.commit()

#cart functions

def add_cart(cust_id):
    gui_globals.conn
    gui_globals.cur
    if not cust_id:
        return None
    gui_globals.cur.execute("SELECT shipping_id FROM shipping WHERE customer_id = %s AND active = TRUE LIMIT 1;",(cust_id,))
    ship_id = gui_globals.cur.fetchone()[0]
    gui_globals.cur.execute("""INSERT INTO cart (cart_id,customer_id,status,shipping_id,card_number)
                            VALUES (DEFAULT,%s,DEFAULT,%s,NULL);""",(cust_id,ship_id))
    gui_globals.cur.execute("SELECT cart_id FROM cart ORDER BY cart_id DESC LIMIT 1;")
    gui_globals.conn.commit()
    return gui_globals.cur.fetchone()[0]

def add_to_cart(cart_id,prod_id,quantity=1):
    gui_globals.conn
    gui_globals.cur
    max_quantity = quantity_in_stock(prod_id, cart_id)
    if max_quantity < 1:
        return 0
    gui_globals.cur.execute("SELECT quantity FROM contains WHERE cart_id = %s AND product_id = %s",(cart_id,prod_id))
    row = gui_globals.cur.fetchone()
    if row is None:
        if quantity > max_quantity:
            quantity = max_quantity
        gui_globals.cur.execute("INSERT INTO contains (product_id,cart_id,quantity) VALUES (%s,%s,%s);",(prod_id,cart_id,quantity))
    else:
        quantity = quantity + row[0]
        if quantity > max_quantity:
            quantity = max_quantity
        modify_cart(cart_id,prod_id,quantity)
        quantity - row[0]
    gui_globals.conn.commit()
    return quantity

def remove_from_cart(cart_id,prod_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("DELETE FROM contains WHERE cart_id = %s AND product_id = %s;",(cart_id,prod_id))
    gui_globals.conn.commit()
    return

def list_cart(cart_id):
    gui_globals.conn
    gui_globals.cur
    if not cart_id:
        return None
    gui_globals.cur.execute("SELECT status FROM cart WHERE cart_id = %s",(cart_id,))
    status = gui_globals.cur.fetchone()[0]
    if status == 'open':
        gui_globals.cur.execute("""SELECT product_id, name, quantity, price FROM cart NATURAL JOIN contains 
                    NATURAL JOIN prices NATURAL JOIN shipping NATURAL JOIN product
                    WHERE cart_id = %s;""",(cart_id,))
    else:
        gui_globals.cur.execute("""SELECT product_id, name, quantity, bought_price, order_date FROM cart NATURAL JOIN contains
                    NATURAL JOIN product WHERE cart_id = %s;""",(cart_id,))
    return gui_globals.cur.fetchall()

def get_cart_total(cart_id):
    gui_globals.conn
    gui_globals.cur
    total = Decimal('0.00')
    result = list_cart(cart_id)
    if result is None:
        return None
    for res in result:
        total = total + (res[2] * res[3])
    return total

def get_num_items(cart_id):
    gui_globals.conn
    gui_globals.cur
    total = 0
    result = list_cart(cart_id)
    if result is None:
        return None
    for res in result:
        total = total + int(res[2])
    return total

def modify_cart(cart_id,prod_id,quantity):
    gui_globals.conn
    gui_globals.cur
    max_quantity = quantity_in_stock(prod_id, cart_id)
    if quantity > max_quantity:
        quantity = max_quantity
    gui_globals.cur.execute("UPDATE contains SET quantity = %s WHERE cart_id = %s AND product_id =%s;",(quantity,cart_id,prod_id))
    gui_globals.conn.commit()
    return quantity

def get_cart_addr(cart_id):
    gui_globals.conn
    gui_globals.cur
    if cart_id is None:
        return None
    gui_globals.cur.execute("SELECT shipping_id FROM cart WHERE cart_id = %s;",(cart_id,))
    row = gui_globals.cur.fetchone()
    if row is None:
        return None
    else:
        return row[0]

def set_cart_addr(cart_id, ship_id):
    gui_globals.conn
    gui_globals.cur
    if cart_id is None or ship_id is None:
        return
    gui_globals.cur.execute("UPDATE cart SET shipping_id = %s WHERE cart_id = %s;",(ship_id,cart_id))
    gui_globals.conn.commit()
    return

def find_open_cart(cust_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT cart_id FROM cart WHERE customer_id = %s AND status = 'open';",(cust_id,))
    row = gui_globals.cur.fetchone()
    if row is None:
        return None
    return row[0]

def submit_order(cart_id,card_num):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT product_id, quantity FROM contains WHERE cart_id = %s;",(cart_id,))
    products = gui_globals.cur.fetchall()
    gui_globals.cur.execute("SELECT state FROM cart NATURAL JOIN shipping WHERE cart_id = %s;",(cart_id,))
    state = gui_globals.cur.fetchone()[0]
    for prod in products:
        quantity = prod[1]
        gui_globals.cur.execute("SELECT warehouse_id, quantity FROM stocks WHERE product_id = %s",(prod[0],))
        wares = gui_globals.cur.fetchall()
        for ware in wares:
            if quantity <= ware[1]:
                modify_stock(prod[0], ware[0], "-" + str(quantity))
                break;
            else:
                modify_stock(prod[0], ware[0], 0)
                quantity = quantity - ware[1]
        gui_globals.cur.execute("SELECT price FROM prices WHERE product_id = %s AND state = %s;",(prod[0],state))
        price = gui_globals.cur.fetchone()[0]
        gui_globals.cur.execute("""UPDATE contains SET bought_price = %s
                        WHERE product_id = %s AND cart_id = %s;""",(price,prod[0],cart_id))
    gui_globals.cur.execute("""UPDATE cart SET status = 'issued', card_number = '%s', order_date = NOW() 
                    WHERE cart_id = %s;""",(card_num,cart_id))
    gui_globals.conn.commit()
    return

def send_order(cart_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("UPDATE cart SET status = 'sent' WHERE cart_id = %s;",(cart_id,))
    gui_globals.conn.commit()
    return

def order_received(cart_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("UPDATE cart SET status = 'received' WHERE cart_id = %s;",(cart_id,))
    gui_globals.conn.commit()
    return

#warehouse functions

def add_warehouse(street,city,state,zipcode,capacity):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""INSERT INTO warehouse (warehouse_id,street,city,state,zip,capacity)
                                VALUES (DEFAULT,%s,%s,%s,%s,%s);""",(street,city,state,zipcode,capacity))
    gui_globals.cur.execute("SELECT warehouse_id FROM warehouse ORDER BY warehouse_id DESC LIMIT 1;")
    gui_globals.conn.commit()
    return gui_globals.cur.fetchone()[0]

def get_warehouses():
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT warehouse_id, street, city, state, zip, capacity FROM warehouse;")
    return gui_globals.cur.fetchall()

def modify_warehouse(ware_id,street=None,city=None,state=None,zipcode=None,capacity=None):
    gui_globals.conn
    gui_globals.cur
    attrs = []
    vals = []
    if street != None:
        attrs.append("street = %s")
        vals.append(street)
    if capacity != None:
        if capacity < vol_in_warehouse(ware_id):
            return False
        attrs.append("capacity = %s")
        vals.append(name)
    if city != None:
        attrs.append("city = %s")
        vals.append(city)
    if state != None:
        attrs.append("state = %s")
        vals.append(state)
    if zipcode != None:
        attrs.append("zip = %s")
        vals.append(zipcode)
    vals.append(ware_id)
    query = "UPDATE warehouse SET "
    for attr in attrs:
        query = query + attr
        if attr != attrs[-1]:
            query = query + ","
        query = query + " "
    query = query + "WHERE warehouse_id = %s;"
    gui_globals.cur.execute(query,tuple(vals));
    gui_globals.conn.commit()
    return True

def del_warehouse(ware_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("DELETE FROM stocks WHERE warehouse_id = %s;",(ware_id,))
    gui_globals.cur.execute("DELETE FROM warehouse WHERE warehouse_id = %s;",(ware_id,))
    gui_globals.conn.commit()
    return

#stock functions

def add_stock(prod_id,ware_id,quantity):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("INSERT INTO stocks (product_id,warehouse_id,quantity) VALUES (%s,%s,%s);",(prod_id,ware_id,0))
    return modify_stock(prod_id, ware_id, quantity)

def quantity_in_stock(prod_id, cart_id):
    gui_globals.conn
    gui_globals.cur
    if cart_id is None:
        return None
    gui_globals.cur.execute("SELECT state FROM cart NATURAL JOIN shipping WHERE cart_id = %s;",(cart_id,))
    state = gui_globals.cur.fetchone()[0]
    gui_globals.cur.execute("""SELECT sum(quantity) FROM stocks NATURAL JOIN warehouse
                    WHERE state = %s AND product_id = %s;""",(state,prod_id))
    q1 = gui_globals.cur.fetchone()[0]
    gui_globals.cur.execute("""SELECT sum(quantity) FROM contains NATURAL JOIN cart WHERE 
                        status = 'open' AND product_id = %s;""",(prod_id,))
    q2 = gui_globals.cur.fetchone()[0]
    if q1 is None:
        q1 = 0
    if q2 is None:
        q2 = 0
    return q1 - q2

def vol_in_warehouse(ware_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("""SELECT volume, quantity FROM product NATURAL JOIN stocks
                    WHERE warehouse_id = %s;""",(ware_id,))
    vol = 0
    rows = gui_globals.cur.fetchall()
    for row in rows:
        vol = vol + (row[0] * row[1])
    return vol

def modify_stock(prod_id,ware_id,quantity):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("SELECT volume FROM product WHERE product_id = %s;",(prod_id,))
    vol_mod = gui_globals.cur.fetchone()[0] * int(quantity)
    gui_globals.cur.execute("SELECT capacity FROM warehouse WHERE warehouse_id = %s;",(ware_id,))
    cap = gui_globals.cur.fetchone()[0]
    if str(quantity)[0] == "+":
        gui_globals.cur.execute("""SELECT quantity FROM stocks
                        WHERE product_id = %s and warehouse_id = %s;""",(prod_id,ware_id))
        quantity = gui_globals.cur.fetchone()[0] + int(str(quantity)[1:])
    elif str(quantity)[0] == "-":
        gui_globals.cur.execute("""SELECT quantity FROM stocks
                        WHERE product_id = %s and warehouse_id = %s;""",(prod_id,ware_id))
        quantity = gui_globals.cur.fetchone()[0] - int(str(quantity)[1:])
    if vol_in_warehouse(ware_id) + vol_mod <= cap: 
        gui_globals.cur.execute("""UPDATE stocks SET quantity = %s 
                        WHERE product_id = %s AND warehouse_id = %s;""",(quantity,prod_id,ware_id))
        gui_globals.conn.commit()
        return True
    return False

def del_stock(prod_id,ware_id):
    gui_globals.conn
    gui_globals.cur
    gui_globals.cur.execute("DELETE FROM stocks WHERE product_id = %s AND warehouse_id = %s;",(prod_id,ware_id))
    gui_globals.conn.commit()

#Application Functions

def connect():
    #gui_globals.connects to database
    gui_globals.conn
    gui_globals.cur
    gui_globals.conn = psycopg2.connect(host="localhost", dbname="storeapp", user="app", password="1234")
    gui_globals.cur = gui_globals.conn.cursor()

def close():
    #commits to database and closes gui_globals.connection
    gui_globals.cur
    gui_globals.conn
    gui_globals.conn.commit()
    gui_globals.cur.close()
    gui_globals.conn.close()

def login(u_id,passw):
    #checks for user and password, if either incorrect, returns (None,None)
    #if valid login, returns matching (customer_id,staff_id)
    gui_globals.cur
    gui_globals.conn
    if not str(u_id).isdigit():
        return(None,None)
    gui_globals.cur.execute("""SELECT * FROM users WHERE user_id = %s AND password = MD5(%s);""",(u_id,passw))
    row = gui_globals.cur.fetchone()
    if row is None:
        return (None,None)
    else:
        return get_ids(u_id)
