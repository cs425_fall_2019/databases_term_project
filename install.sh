#!/bin/bash

#  @file:      install.sh
#  @author:    sfarnsworth, <sfarnsworth@hawk.iit.edu>
#  @created:   09.12.2019
#  @updated:   11.20.2019
#
#  Intended for use on UBUNUTU 18.04+ LTS. This will install all required dependencies.
#  A new superuser with your username will be made for postgres
#  and a test database named 'university' will be created and populated if requested.
#
#  Discrete packages to be installed:
#  python3
#  python3-dev
#  python3-psycopg2 -- postgresql porcelain
#  python3-kivy     -- opensource multi-platform GUI toolkit
#  postgresql-11
#  libpq-dev
#

if ! [ $(id -u) = 0 ]; then
    echo 'Root pivileges required. Run as root'
    exit 1
fi

REPO_list=/etc/apt/sources.list.d/pgdg.list
REPO='deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main'
REPO_key=https://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc

KIVY_WHEEL=https://kivy.org/downloads/appveyor/kivy/Kivy-2.0.0.dev0-cp36-cp36m-win_amd64.whl

DDL=https://www.db-book.com/db7/university-lab-dir/sample_tables-dir/DDL.sql
RELATIONS=https://www.db-book.com/db7/university-lab-dir/sample_tables-dir/largeRelations/largeRelationsInsertFile.sql

# Add repository for postgresql-11 (UBUNTU LTS 18.04 Bionic)
echo '>>> Adding PostgreSQL repository to the apt repositories list'
touch $REPO_list
echo $REPO >> $REPO_list
wget --quiet -O - $REPO_key | apt-key add -

# Update and install necessary software
echo '>>> Updating apt and installing required software'
apt update
apt-get install -y python3 python3-dev postgresql-11 libpq-dev
sudo python3 -m pip install --upgrade pip wheel setuptools virtualenv
sudo python3 -m pip install kivy psycopg2


# Start postgres cluster (on VM only, comment out line for native Ubuntu)
pg_ctlcluster 11 main start

# Add your username to postgres as superuser
echo '>>> Creating superuser on postgres'
sudo -su postgres createuser -s $(whoami)

# Add test database to postgres?
read -p 'Create the university test database?(y/n): '
if [ $REPLY = 'y' ]; then
    echo '>>> Creating university database...(this will take a while)'
    createdb university
    wget --quiet -O - $DDL | psql university --quiet --echo-hidden -f -
    wget --quiet -O - $RELATIONS | psql university --quiet --echo-hidden -f -
fi
echo "Setup has been completed!"
