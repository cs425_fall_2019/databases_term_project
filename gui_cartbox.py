
import os
import sql
import kivy
import gui_globals
from gui_genericbox import generic_box
from gui_separator import separator
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.graphics import *
kivy.require("1.11.1")

sidebar_size_x = 160
loginbox_size_y = 140
userbox_size_y = 80
cartbox_size_y = 110
filterbox_size_y = 35
searchbar_size_y = 35
default_line_height = 25

class cartbox(GridLayout):

    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        
        self.rows = 5
        self.cols = 1
        self.size_hint = (None,None)
        self.size = (sidebar_size_x,cartbox_size_y)
        self.padding = 4

        self.box1 = generic_box(sidebar_size_x-8,default_line_height)
        self.box2 = generic_box(sidebar_size_x-8,5)
        self.box3 = generic_box(sidebar_size_x-8,default_line_height)
        self.box4 = generic_box(sidebar_size_x-8,default_line_height)
        self.box5 = generic_box(sidebar_size_x-8,default_line_height)

        self.add_widget(self.box1)
        self.add_widget(self.box2)
        self.add_widget(self.box3)
        self.add_widget(self.box4)
        self.add_widget(self.box5)
        
        self.cart_header = Label(text="Cart",
                                 size_hint=(1,1),
                                 text_size=self.size,
                                 halign="left",
                                 valign="center",
                                 padding=(5,2),
                                 color=(0,0,0,1))
        self.cart_login_err = Label(text="Login to add items or view orders/carts",
                                    size_hint=(1,1),
                                    text_size=self.size,
                                    halign="left",
                                    valign="center",
                                    padding = (5,2),
                                    color=(0.75,0,0,0.85),
                                    font_size=10,
                                    bold=True)
        self.staff_message = Label(text="Login as a Customer to add items or view orders/carts",
                                    size_hint=(1,1),
                                    text_size=self.size,
                                    halign="left",
                                    valign="center",
                                    padding = (5,2),
                                    color=(0.75,0,0,0.85),
                                    font_size=10,
                                    bold=True)
        self.cart_items = Label(text=f'Items: {1}',
                                     size_hint=(1,1),
                                     text_size=self.size,
                                     halign="left",
                                     valign="center",
                                     padding=(5,8),
                                     color=(0,0,0,0.85),
                                     font_size=12,
                                     bold=True)
        self.cart_balance = Label(text=f'Balance: ${sql.get_cart_total(gui_globals.cart_id)}', #
                                     size_hint=(1,1),
                                     text_size=self.size,
                                     halign="left",
                                     valign="center",
                                     padding=(5,8),
                                     color=(0,0,0,0.85),
                                     font_size=12,
                                     bold=True)
        self.view_cart_btn = Button(text="View cart",font_size=12)
        
        self.box1.add_widget(self.cart_header)
        self.box2.add_widget(separator(2,None,0.75,0.75,0.75,1))
        self.box3.add_widget(self.cart_login_err)
        
        with self.canvas.before:
            Color(1,1,1,1)
            self.rect = Rectangle(size=self.size, pos=self.pos)
            self.bind(pos=self.update_rect, size=self.update_rect)

    def update_rect(self,*args):
        self.rect.pos = self.pos
        self.rect.size = self.size
    
