
import os
import sql
import kivy
import gui_globals
from gui_genericbox import generic_box
from gui_separator import separator
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.gridlayout import GridLayout
from kivy.uix.dropdown import DropDown
from kivy.graphics import *
kivy.require("1.11.1")

workspace_size_x = 550
sidebar_size_x = 160
page_size_y = 200
loginbox_size_y = 80
userbox_size_y = 80
cartbox_size_y = 110
filterbox_size_y = 35
searchbar_size_y = 35
default_line_height = 25

class page_select(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.cols = 1
        self.rows = 1
        self.size_hint = (None,None)
        self.size = (110,default_line_height)
        self.padding = 0

        self.box = generic_box(100,default_line_height-10)
        self.box.cols = 7
        self.box.size_hint = (1,None)
        self.box.size = (0,default_line_height)

        self.add_widget(self.box)

        self.ip_goto_page = TextInput(text="1",
                                      multiline=False,
                                      size_hint=(None,1),
                                      width=30,
                                      font_size=10)
        self.goto_page_submit = Button(text="Go",width=15,font_size=10)
        self.goto_msg = Label(text=f' / {gui_globals.product_num_pages} pages',
                              size_hint=(1,1),
                              text_size=self.size,
                              halign="left",
                              valign="center",
                              padding=(5,2),
                              font_size=10,
                              color=(0,0,0,1))

        self.box.add_widget(self.ip_goto_page)
        self.box.add_widget(separator(None,50,1,1,1,1))
        self.box.add_widget(self.goto_msg)
        self.box.add_widget(separator(None,5,1,1,1,1))
        self.box.add_widget(self.goto_page_submit)

class product_box(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.cols = 5
        self.rows = 1
        self.size_hint = (None,None)
        self.size = (300,default_line_height)
        self.padding = 0

        self.boxes = []
        for i in range(4):
            self.boxes.append(generic_box(100,default_line_height))
            self.add_widget(self.boxes[i])

        self.boxes[3].cols = 3
        self.boxes[3].size_hint = (1,None)
        self.boxes[3].size = (0,default_line_height)
        
        self.product_title = Label(text="Product title",
                                   size_hint=(1,1),
                                   text_size=self.size,
                                   halign="left",
                                   valign="center",
                                   padding=(5,2),
                                   font_size=10,
                                   color=(0,0,0,1))
        self.product_price = Label(text="Price: ${1}",
                                   size_hint=(1,1),
                                   text_size=self.size,
                                   halign="left",
                                   valign="center",
                                   padding=(5,2),
                                   font_size=10,
                                   color=(0,0,0,1))
        self.product_stock = Label(text="Stock: {1}",
                                   size_hint=(1,1),
                                   text_size=self.size,
                                   halign="left",
                                   valign="center",
                                   padding=(5,2),
                                   font_size=10,
                                   color=(0,0,0,1))
        self.ip_product_add = TextInput(hint_text="#",
                                        multiline=False,
                                        size_hint=(None,1),
                                        width=30,
                                        font_size=10)
        self.cart_add = Button(text="Add to cart",
                               size_hint=(None,1),
                               width=55,
                               font_size=10)
        
        self.boxes[0].add_widget(self.product_title)
        self.boxes[1].add_widget(self.product_price)
        self.boxes[2].add_widget(self.product_stock)

        self.boxes[3].add_widget(self.ip_product_add)
        self.boxes[3].add_widget(separator(None,10,0.85,0.85,0.85,1))
        self.boxes[3].add_widget(self.cart_add)
        
        with self.canvas.before:
            Color(0.85,0.85,0.85,1)
            self.rect = Rectangle(size=self.size, pos=self.pos)
            self.bind(pos=self.update_rect, size=self.update_rect)

    def update_rect(self,*args):
        self.rect.pos = self.pos
        self.rect.size = self.size

class staff_product_box(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.cols = 5
        self.rows = 1
        self.size_hint = (None,None)
        self.size = (300,default_line_height)
        self.padding = 0

        self.boxes = []
        for i in range(4):
            self.boxes.append(generic_box(100,default_line_height))
            self.add_widget(self.boxes[i])

        self.boxes[3].cols = 2
        self.boxes[3].size_hint = (1,None)
        self.boxes[3].size = (0,default_line_height)
        
        self.product_title = Label(text="Product title",
                                   size_hint=(1,1),
                                   text_size=self.size,
                                   halign="left",
                                   valign="center",
                                   padding=(5,2),
                                   font_size=10,
                                   color=(0,0,0,1))
        self.product_price = Label(text="",
                                   size_hint=(1,1),
                                   text_size=self.size,
                                   halign="left",
                                   valign="center",
                                   padding=(5,2),
                                   font_size=10,
                                   color=(0,0,0,1))
        self.product_stock = Label(text="",
                                   size_hint=(1,1),
                                   text_size=self.size,
                                   halign="left",
                                   valign="center",
                                   padding=(5,2),
                                   font_size=10,
                                   color=(0,0,0,1))
        self.product_select = Button(text="Select",
                               size_hint=(None,1),
                               width=55,
                               font_size=10)
        
        self.boxes[0].add_widget(self.product_title)
        self.boxes[1].add_widget(self.product_price)
        self.boxes[2].add_widget(self.product_stock)

        self.boxes[3].add_widget(separator(None,10,0.85,0.85,0.85,1))
        self.boxes[3].add_widget(self.product_select)
        
        with self.canvas.before:
            Color(0.85,0.85,0.85,1)
            self.rect = Rectangle(size=self.size, pos=self.pos)
            self.bind(pos=self.update_rect, size=self.update_rect)

    def update_rect(self,*args):
        self.rect.pos = self.pos
        self.rect.size = self.size
        

class page_products(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(*kwargs)

        self.rows = 15
        self.cols = 1
        self.size_hint = (None,None)
        self.size = (workspace_size_x,500)

       # landing page initialization
        gui_globals.filt_products = sql.search_products(gui_globals.search, gui_globals.cat_id, gui_globals.cart_id)
        gui_globals.product_num_products = len(gui_globals.filt_products) - 1
        if gui_globals.product_num_products < 0:
            gui_globals.product_num_products = 0
        gui_globals.product_num_pages = gui_globals.product_num_products // 10 + 1

        self.boxes = []
        for i in range(4):
            if i == 1:
                self.boxes.append(generic_box(workspace_size_x,8))
            else:
                self.boxes.append(generic_box(workspace_size_x,default_line_height))
            self.add_widget(self.boxes[i])

        self.page_goto = page_select() 
        self.product_page_header = Label(text="Products",
                                         size_hint=(1,1),
                                         text_size=self.size,
                                         halign="left",
                                         valign="center",
                                         padding=(5,2),
                                         color=(0,0,0,1),
                                         bold=True)

        self.boxes[0].add_widget(self.product_page_header)
        self.boxes[1].add_widget(separator(2,100,0.75,0.75,0.75,1))
        self.boxes[2].add_widget(self.page_goto)

        self.product_boxes = []
        self.lines = []

        for i in range(10):
            if i + gui_globals.product_page_indexer > gui_globals.product_num_products:
                break
            self.lines.append(generic_box(workspace_size_x-8,default_line_height+10))
            self.lines[i].padding = (125,0)
            self.add_widget(self.lines[i])
            self.product_boxes.append(product_box())
            self.lines[i].add_widget(self.product_boxes[i])
            self.product_boxes[i].product_title.text= gui_globals.filt_products[i + gui_globals.product_page_indexer][1]
            self.product_boxes[i].product_price.text= f'Login as Customer to see prices/stock'
            self.product_boxes[i].product_stock.text= f''


class cart_checkout(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.cols = 1
        self.rows = 1
        self.size_hint = (None,None)
        self.size = (workspace_size_x,default_line_height)
        self.padding = 0

        self.box = generic_box(100,default_line_height+10)
        self.box.cols = 3
        self.box.size_hint = (1,None)
        self.box.size = (0,default_line_height)

        self.add_widget(self.box)

        self.cart_shipping_dropdown = DropDown()
        self.cart_shipping_dropdown_btn = Button(text="--Select shipping--",size_hint=(None,None),size=(200,default_line_height),font_size=11)
        self.cart_credit_card_dropdown = DropDown()
        self.cart_credit_card_dropdown_btn = Button(text="--Select payment--",size_hint=(None,None),size=(200,default_line_height),font_size=11)
        self.cart_submit_btn = Button(text="Submit order",size_hint=(None,None),size=(140,default_line_height),font_size=11)
            
            
class cart_box(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.cols = 5
        self.rows = 1
        self.size_hint = (None,None)
        self.size = (300,default_line_height)
        self.padding = 0

        self.boxes = []
        for i in range(4):
            self.boxes.append(generic_box(100,default_line_height))
            self.add_widget(self.boxes[i])

        self.boxes[3].cols = 3
        self.boxes[3].size_hint = (1,None)
        self.boxes[3].size = (0,default_line_height)
        
        self.product_title = Label(text="Product title",
                                   size_hint=(1,1),
                                   text_size=self.size,
                                   halign="left",
                                   valign="center",
                                   padding=(5,2),
                                   font_size=10,
                                   color=(0,0,0,1))
        self.product_price = Label(text="Price: ${1}",
                                   size_hint=(1,1),
                                   text_size=self.size,
                                   halign="left",
                                   valign="center",
                                   padding=(5,2),
                                   font_size=10,
                                   color=(0,0,0,1))
        self.product_stock = Label(text="Quantity: {1}",
                                   size_hint=(1,1),
                                   text_size=self.size,
                                   halign="left",
                                   valign="center",
                                   padding=(5,2),
                                   font_size=10,
                                   color=(0,0,0,1))
        self.ip_product_add = TextInput(hint_text="#",
                                        multiline=False,
                                        size_hint=(None,1),
                                        width=30,
                                        font_size=10)
        self.cart_add = Button(text="Update",
                               size_hint=(None,1),
                               width=55,
                               font_size=10)
        
        self.boxes[0].add_widget(self.product_title)
        self.boxes[1].add_widget(self.product_price)
        self.boxes[2].add_widget(self.product_stock)

        self.boxes[3].add_widget(self.ip_product_add)
        self.boxes[3].add_widget(separator(None,10,0.85,0.85,0.85,1))
        self.boxes[3].add_widget(self.cart_add)
        
        with self.canvas.before:
            Color(0.85,0.85,0.85,1)
            self.rect = Rectangle(size=self.size, pos=self.pos)
            self.bind(pos=self.update_rect, size=self.update_rect)

    def update_rect(self,*args):
        self.rect.pos = self.pos
        self.rect.size = self.size
            

class page_cart_overview(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.rows = 16
        self.cols = 1
        self.size_hint = (None,None)
        self.size = (workspace_size_x,500)

        self.boxes = []
        for i in range(6):
            if i == 1 or i == 3:
                self.boxes.append(generic_box(workspace_size_x,4))
            elif i == 5:
                self.boxes.append(generic_box(workspace_size_x,default_line_height+8))
            else:
                self.boxes.append(generic_box(workspace_size_x,default_line_height))
            self.add_widget(self.boxes[i])

        self.page_goto = page_select() 
        self.cart_page_header = Label(text="Cart",
                                         size_hint=(1,1),
                                         text_size=self.size,
                                         halign="left",
                                         valign="center",
                                         padding=(5,2),
                                         color=(0,0,0,1),
                                         bold=True)
        self.cart_checkout_line = cart_checkout()

        self.boxes[0].add_widget(self.cart_page_header)
        self.boxes[1].add_widget(separator(2,100,0.75,0.75,0.75,1))
        self.boxes[2].add_widget(self.cart_checkout_line)
        self.boxes[3].add_widget(separator(2,100,0.75,0.75,0.75,1))
        self.boxes[5].add_widget(self.page_goto)

        self.cart_boxes = []
        self.lines = []

        for i in range(10):
            if i + gui_globals.cart_page_indexer > gui_globals.curr_cart_size:
                break
            self.lines.append(generic_box(workspace_size_x-8,default_line_height+8))
            self.lines[i].padding = (125,0)
            self.cart_boxes.append(cart_box())
            self.lines[i].add_widget(self.cart_boxes[i])

            
class info_form(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.rows = 7
        self.cols = 1
        self.size_hint = (None,None)
        self.size = (workspace_size_x,200)

        self.boxes = []
        for i in range(7):
            self.boxes.append(generic_box(workspace_size_x-8,default_line_height))
            self.boxes[i].cols = 2
            self.add_widget(self.boxes[i])

        self.info_street = Label(text="Street:",size_hint=(None,1),text_size=(50,0),font_size=11,
                                    halign="left",valign="center",padding=(5,5),color=(0,0,0,1))
        self.ip_info_street = TextInput(text="street_name",multiline=False,size_hint_x=None,width=150,font_size=10)
        self.info_apt = Label(text="Apt:",size_hint=(None,1),text_size=(50,0),font_size=11,
                                    halign="left",valign="center",padding=(5,5),color=(0,0,0,1))
        self.ip_info_apt = TextInput(text="apt_number",multiline=False,size_hint_x=None,width=150,font_size=10)
        self.info_city = Label(text="City:",size_hint=(None,1),text_size=(50,0),font_size=11,
                                    halign="left",valign="center",padding=(5,5),color=(0,0,0,1))
        self.ip_info_city = TextInput(text="city_name",multiline=False,size_hint_x=None,width=150,font_size=10)
        self.info_state = Label(text="State:",size_hint=(None,1),text_size=(50,0),font_size=11,
                                    halign="left",valign="center",padding=(5,5),color=(0,0,0,1))
        self.ip_info_state = TextInput(text="state_name",multiline=False,size_hint_x=None,width=150,font_size=10)
        self.info_zip = Label(text="Zip:",size_hint=(None,1),text_size=(50,0),font_size=11,
                                    halign="left",valign="center",padding=(5,5),color=(0,0,0,1))
        self.ip_info_zip = TextInput(text="zip_code",multiline=False,size_hint_x=None,width=150,font_size=10)
        self.info_card = Label(text="Card:",size_hint=(None,1),text_size=(50,0),font_size=11,
                                    halign="left",valign="center",padding=(5,5),color=(0,0,0,1))
        self.ip_info_card = TextInput(text="card_number",multiline=False,size_hint_x=None,width=150,font_size=10)
        self.info_remove = Button(text="Remove",font_size=11)
        self.info_submit = Button(text="Submit",font_size=11)

        self.boxes[0].add_widget(self.info_street)
        self.boxes[0].add_widget(self.ip_info_street)
        self.boxes[1].add_widget(self.info_apt)
        self.boxes[1].add_widget(self.ip_info_apt)
        self.boxes[2].add_widget(self.info_city)
        self.boxes[2].add_widget(self.ip_info_city)
        self.boxes[3].add_widget(self.info_state)
        self.boxes[3].add_widget(self.ip_info_state)
        self.boxes[4].add_widget(self.info_zip)
        self.boxes[4].add_widget(self.ip_info_zip)
        self.boxes[5].add_widget(self.info_card)
        self.boxes[5].add_widget(self.ip_info_card)
        self.boxes[6].add_widget(self.info_remove)
        self.boxes[6].add_widget(self.info_submit)
        
        
class page_customer_account(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.rows = 21
        self.cols = 1
        self.size_hint = (None,None)
        self.size = (workspace_size_x,500)

        self.boxes = []
        for i in range(21):
            if i == 1 or i == 3 or i == 8 or i == 10 or i ==12 or i == 14 or i == 16 or i == 18:
                self.boxes.append(generic_box(workspace_size_x,8))
            else:
                self.boxes.append(generic_box(workspace_size_x,default_line_height))
            self.add_widget(self.boxes[i])
            
        self.cust_account_page_header = Label(text="Customer Account",size_hint=(1,1),text_size=self.size,font_size=14,
                                              halign="left",valign="center",padding=(5,2),color=(0,0,0,1),bold=True)
        self.cust_basic_info_header = Label(text="Basic Info",size_hint=(1,1),text_size=self.size,font_size=12,
                                            halign="left",valign="center",padding=(5,2),color=(0,0,0,1),bold=True)
        self.cust_shipping_info_header = Label(text="Shipping",size_hint=(1,1),text_size=self.size,font_size=12,
                                               halign="left",valign="center",padding=(5,2),color=(0,0,0,1),bold=True)
        self.cust_payment_info_header = Label(text="Payments",size_hint=(1,1),text_size=self.size,font_size=12,
                                              halign="left",valign="center",padding=(5,2),color=(0,0,0,1),bold=True)
        self.cust_edit_selection_header = Label(text="Edit Selection",size_hint=(1,1),text_size=self.size,font_size=12,
                                                halign="left",valign="center",padding=(5,2),color=(0,0,0,1),bold=True)
        self.cust_first_name = Label(text=f'Name:                 {"John"}',size_hint=(1,1),text_size=self.size,font_size=11,
                                     halign="left",valign="center",padding=(5,10),color=(0,0,0,1))
        self.cust_last_name = Label(text=f'Surname:            {"Doe"}',size_hint=(1,1),text_size=self.size,font_size=11,
                                    halign="left",valign="center",padding=(5,10),color=(0,0,0,1))
        self.cust_id = Label(text=f'Customer ID:      {"0001293"}',size_hint=(1,1),text_size=self.size,font_size=11,
                                    halign="left",valign="center",padding=(5,10),color=(0,0,0,1))
        self.cust_outstanding_balance = Label(text=f'',size_hint=(1,1),text_size=self.size,font_size=11,
                                    halign="left",valign="center",padding=(5,10),color=(0,0,0,1))
        self.edit_info_form = info_form()
        
        self.cust_shipping_dropdown = DropDown()
        self.cust_shipping_dropdown_btn = Button(text="--Click to select/edit shipping addresses--",
                                                 size_hint=(None,None),size=(workspace_size_x-10,default_line_height),font_size=11)
        self.cust_payment_dropdown = DropDown()
        self.cust_payment_dropdown_btn = Button(text="--Click to select/edit payments--",
                                                size_hint=(None,None),size=(workspace_size_x-10,default_line_height),font_size=11)
                                      
        self.boxes[0].add_widget(self.cust_account_page_header)
        self.boxes[1].add_widget(separator(2,150,0.75,0.75,0.75,1))
        self.boxes[2].add_widget(self.cust_basic_info_header)
        self.boxes[3].add_widget(separator(2,100,0.75,0.75,0.75,1))
        self.boxes[4].add_widget(self.cust_first_name)
        self.boxes[5].add_widget(self.cust_last_name)
        self.boxes[6].add_widget(self.cust_id)
        self.boxes[7].add_widget(self.cust_outstanding_balance)
        self.boxes[8].add_widget(separator(2,0,1,1,1,1))
        self.boxes[9].add_widget(self.cust_shipping_info_header)
        self.boxes[10].add_widget(separator(2,100,0.75,0.75,0.75,1))
        self.boxes[12].add_widget(separator(2,0,1,1,1,1))
        self.boxes[13].add_widget(self.cust_payment_info_header)
        self.boxes[14].add_widget(separator(2,100,0.75,0.75,0.75,1))
        self.boxes[16].add_widget(separator(2,0,1,1,1,1))
        self.boxes[17].add_widget(self.cust_edit_selection_header)
        self.boxes[18].add_widget(separator(2,100,0.75,0.75,0.75,1))
        self.boxes[19].add_widget(self.edit_info_form)
        

class page_staff_control(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.rows = 22
        self.cols = 1
        self.size_hint = (None,None)
        self.size = (workspace_size_x,600)

        gui_globals.all_products = sql.search_products(None,None,None)
        gui_globals.staff_num_products = len(gui_globals.all_products) - 1
        if gui_globals.staff_num_products < 0:
            gui_globals.staff_num_products = 0
        gui_globals.staff_num_pages = gui_globals.staff_num_products // 5 + 1

        self.boxes = []
        for i in range(5):
            if i == 1 or i == 3:
                self.boxes.append(generic_box(workspace_size_x,8))
            else:
                self.boxes.append(generic_box(workspace_size_x,default_line_height))
            self.add_widget(self.boxes[i])

        self.lines = []
        self.staff_product_boxes = []
        for i in range(5):
            if i + gui_globals.staff_page_indexer > gui_globals.staff_num_products:
                break
            self.lines.append(generic_box(workspace_size_x,default_line_height+8))
            self.lines[i].padding = (140,0)
            self.staff_product_boxes.append(staff_product_box())
            self.lines[i].add_widget(self.staff_product_boxes[i])
            self.add_widget(self.lines[i])

        for i in range(5,11):
            if i == 7:
                self.lines.append(generic_box(workspace_size_x,8))
            else:
                self.lines.append(generic_box(workspace_size_x,default_line_height))
                self.lines[i].padding = (0,0)
            self.add_widget(self.lines[i])

        self.price_box = generic_box(50,default_line_height)
        self.price_box.cols = 4

        self.stock_box = generic_box(100,default_line_height)
        self.stock_box.cols = 5
                                
        self.staff_control_page_header = Label(text="Staff Controls",
                                               size_hint=(1,1),
                                               text_size=self.size,
                                               font_size=14,
                                               halign="left",
                                               valign="center",
                                               padding=(5,2),
                                               color=(0,0,0,1),
                                               bold=True)
        self.product_select_header = Label(text="Product Select",
                                           size_hint=(1,1),
                                           text_size=self.size,
                                           font_size=12,
                                           halign="left",
                                           valign="center",
                                           padding=(5,2),
                                           color=(0,0,0,1),
                                           bold=True)
        self.product_modify_header = Label(text="Product Modify",
                                           size_hint=(1,1),
                                           text_size=self.size,
                                           font_size=12,
                                           halign="left",
                                           valign="center",
                                           padding=(5,2),
                                           color=(0,0,0,1),
                                           bold=True)
        self.page_goto = page_select()
        self.selected_product = Label(text=f'Product:   {"product_name"} ({"pid"})',
                                        size_hint=(1,1),
                                        text_size=self.size,
                                        font_size=11,
                                        halign="left",
                                        valign="center",
                                        padding=(5,2),
                                        color=(0,0,0,1))
        self.price_box_label = Label(text="Price: ",
                                        size_hint=(None,1),
                                        text_size=(50,0),
                                        font_size=11,
                                        halign="left",
                                        valign="center",
                                        padding=(5,2),
                                        color=(0,0,0,1))
        self.ip_price_update = TextInput(hint_text="$",multiline=False,size_hint_x=None,width=35,font_size=10)
        self.ip_state_update = TextInput(hint_text="ST",multiline=False,size_hint_x=None,width=35,font_size=10)
        self.price_update_btn = Button(text="Update",size_hint_x=None,width=50,font_size=10)
        self.stock_box_label = Label(text="Stock: ",
                                        size_hint=(None,1),
                                        text_size=(50,0),
                                        font_size=11,
                                        halign="left",
                                        valign="center",
                                        padding=(5,2),
                                        color=(0,0,0,1))
        self.ip_stock_update = TextInput(hint_text="#",multiline=False,size_hint_x=None,width=35,font_size=10)
        self.stock_update_btn = Button(text="Add",size_hint_x=None,width=50,font_size=10)
        self.stock_supplier_dropdown_btn = Button(text="--select supplier--",size_hint_x=None,width=175,font_size=10)
        self.stock_warehouse_dropdown_btn = Button(text="--select warehouse--",size_hint_x=None,width=175,font_size=10)

        self.stock_supplier_dropdown = DropDown()
        self.stock_warehouse_dropdown = DropDown()

        self.boxes[0].add_widget(self.staff_control_page_header)
        self.boxes[1].add_widget(separator(2,150,0.75,0.75,0.75,1))
        self.boxes[2].add_widget(self.product_select_header)
        self.boxes[3].add_widget(separator(2,75,0.75,0.75,0.75,1))
        self.boxes[4].add_widget(self.page_goto)

        self.lines[6].add_widget(self.product_modify_header)
        self.lines[7].add_widget(separator(2,75,0.75,0.75,0.75,1))
        self.lines[8].add_widget(self.selected_product)
        self.lines[9].add_widget(self.price_box)
        self.lines[10].add_widget(self.stock_box)

        self.price_box.add_widget(self.price_box_label)
        self.price_box.add_widget(self.ip_price_update)
        self.price_box.add_widget(self.ip_state_update)
        self.price_box.add_widget(self.price_update_btn)
        self.stock_box.add_widget(self.stock_box_label)
        self.stock_box.add_widget(self.ip_stock_update)
        self.stock_box.add_widget(self.stock_update_btn)

        
        
        
