def init():
	# general
	global u_id
	global c_id
	global s_id
	global cat_id
	global cart_id
	global conn
	global cur

	# filter and search
	global filt_products
	global search

	# cart
	global curr_cart
	global curr_cart_size
	global cart_page_indexer
	global cart_num_pages
	global curr_card_list
	global curr_shipping_list

	# products
	global product_page_indexer
	global product_num_products
	global product_num_pages

	# customer account
	global shipping_or_payment
	global ship_id
	global card_id

	# staff controls
	global staff_page_indexer
	global staff_num_pages
	global staff_num_products
	global all_products
	global prod_id
	global supp_id
	global wh_id

	# paging
	global curr_page
	global state_list

	u_id = None
	c_id = None
	s_id = None
	cat_id = None
	cart_id = None
	conn = None
	cur = None
	filt_products = []
	search = None
	curr_cart = []
	curr_cart_size = 0
	cart_page_indexer = 0
	cart_num_pages = 0
	curr_card_list = []
	curr_shipping_list = []
	# pages are 'products', 'cart', 'customer' or 'staff'
	curr_page = 'products'
	shipping_or_payment = ''
	ship_id = 0
	card_id = 0
	staff_page_indexer = 0
	staff_num_pages = 0
	staff_num_products = 0
	product_page_indexer = 0
	product_num_products = 0
	all_products = []
	prod_id = 0
	supp_id = 0
	wh_id = 0
	product_num_pages = 0
	state_list = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]

