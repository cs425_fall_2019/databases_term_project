
import os
import sql
import kivy
import gui_globals
from gui_genericbox import generic_box
from gui_sidebar import sidebar
from gui_loginbox import loginbox
from gui_cartbox import cartbox
from gui_filterbox import filterbox
from gui_searchbar import searchbar
from gui_workspace import workspace
from gui_pages import *
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.graphics import *
kivy.require("1.11.1")

workspace_size_x = 550
page_size_y = 400
sidebar_size_x = 160
loginbox_size_y = 140
userbox_size_y = 80
cartbox_size_y = 110
filterbox_size_y = 35
searchbar_size_y = 35
default_line_height = 25

gui_globals.init()

class landing(FloatLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        sql.connect()

        self.sidebar_container = sidebar()
        self.sidebar_container.pos = (10,485)

        self.workspace_container = workspace()
        self.workspace_container.pos = (176,63)

        self.size = (1000,1000)
        self.add_widget(self.sidebar_container)
        self.add_widget(self.workspace_container)

        # workspace page button binds
        self.workspace_container.products.page_goto.goto_page_submit.bind(on_release=self.display_products_page)
        self.workspace_container.cart.page_goto.goto_page_submit.bind(on_release=self.display_cart_page)
        self.workspace_container.staff_controls.page_goto.goto_page_submit.bind(on_release=self.display_staff_portal_page)
        self.workspace_container.staff_controls.price_update_btn.bind(on_release=self.price_update)
        self.workspace_container.staff_controls.stock_update_btn.bind(on_release=self.stock_update)

        # create default sidebar objects
        # create login in sidebar
        self.sidebar_container.login_container = loginbox()
        self.sidebar_container.login_container.login_submit.bind(on_press=self.login)
        self.sidebar_container.login_container.logout_submit.bind(on_press=self.logout)
        self.sidebar_container.login_container.account_info.bind(on_press=self.display_my_account_page)
        self.sidebar_container.login_container.staff_portal.bind(on_press=self.display_staff_portal_page)
        
        #create cartbox in sidebar
        self.sidebar_container.cart_container = cartbox()
        self.sidebar_container.cart_container.view_cart_btn.bind(on_press=self.display_cart_page)

        # create filterbox in sidebar
        self.sidebar_container.filterbox_container = filterbox()

        #create searchbar in sidebar
        self.sidebar_container.searchbar_container = searchbar()
        self.sidebar_container.searchbar_container.search_submit.bind(on_press=self.search_submit)
        self.sidebar_container.searchbar_container.search_submit.bind(on_release=self.display_products_page)

        # add objects to sidebar boxes
        self.sidebar_container.box1.add_widget(self.sidebar_container.login_container)
        self.sidebar_container.box2.add_widget(self.sidebar_container.cart_container)
        self.sidebar_container.box3.add_widget(self.sidebar_container.searchbar_container)
        self.sidebar_container.box4.add_widget(self.sidebar_container.filterbox_container)
        
    def login(self,instance):
        print(f'login request:: attempting login >> \
                "{self.sidebar_container.login_container.ip_username.text}:{self.sidebar_container.login_container.ip_passwd.text}"')

        # get user credentials
        login_username = self.sidebar_container.login_container.ip_username.text
        login_passwd = self.sidebar_container.login_container.ip_passwd.text
        id_tup = sql.login(login_username, login_passwd)
        gui_globals.c_id = id_tup[0]
        gui_globals.s_id = id_tup[1]
        gui_globals.u_id = login_username

        # if user is a customer, grab a cart
        gui_globals.cart_id = self.associate_cart(gui_globals.c_id)

        gui_globals.filt_products = sql.search_products(gui_globals.search, gui_globals.cat_id, gui_globals.cart_id)

        self.init_sidebar_login()

    def logout(self,instance):
        print(f'logout request:: attempting login >> \
                "{self.sidebar_container.login_container.ip_username.text}:{self.sidebar_container.login_container.ip_passwd.text}"')
        gui_globals.c_id = None
        gui_globals.s_id = None
        gui_globals.s_id = None
        cat_id = None
        gui_globals.cart_id = None  

        self.init_sidebar_logout()

    def init_sidebar_login(self):
        self.sidebar_container.login_container.ip_username.text = ""
        self.sidebar_container.login_container.ip_passwd.text= ""
        if (gui_globals.c_id and gui_globals.s_id):
            self.sidebar_container.login_container.box1.clear_widgets()
            self.sidebar_container.login_container.box2.clear_widgets()            
            self.sidebar_container.login_container.box3.clear_widgets()
            self.sidebar_container.login_container.box4.clear_widgets()
            self.sidebar_container.login_container.box5.clear_widgets()
            self.sidebar_container.cart_container.box3.clear_widgets()
            self.sidebar_container.cart_container.box4.clear_widgets()
            self.sidebar_container.cart_container.box5.clear_widgets()     
            self.sidebar_container.login_container.box1.add_widget(self.sidebar_container.login_container.welcome_msg)       
            self.sidebar_container.login_container.box3.add_widget(self.sidebar_container.login_container.account_info)
            self.sidebar_container.login_container.box4.add_widget(self.sidebar_container.login_container.staff_portal)
            self.sidebar_container.login_container.box5.add_widget(self.sidebar_container.login_container.logout_submit)
            self.sidebar_container.cart_container.box3.add_widget(self.sidebar_container.cart_container.cart_items)
            self.sidebar_container.cart_container.box4.add_widget(self.sidebar_container.cart_container.cart_balance)
            self.sidebar_container.cart_container.box5.add_widget(self.sidebar_container.cart_container.view_cart_btn)
            self.display_products_page(0)
            self.sidebar_container.login_container.welcome_msg.text = f'Welcome, {sql.get_name(gui_globals.u_id)[0]}'
            self.sidebar_container.cart_container.cart_items.text = f'Items: {sql.get_num_items(gui_globals.cart_id)}'
            self.sidebar_container.cart_container.cart_balance.text = f'Balance: ${sql.get_cart_total(gui_globals.cart_id)}'
        elif gui_globals.s_id and not gui_globals.c_id:
            self.sidebar_container.login_container.box1.clear_widgets()
            self.sidebar_container.login_container.box2.clear_widgets()
            self.sidebar_container.login_container.box3.clear_widgets()
            self.sidebar_container.login_container.box4.clear_widgets()
            self.sidebar_container.login_container.box5.clear_widgets()
            self.sidebar_container.cart_container.box3.clear_widgets()
            self.sidebar_container.cart_container.box4.clear_widgets()
            self.sidebar_container.cart_container.box5.clear_widgets()
            self.sidebar_container.login_container.box1.add_widget(self.sidebar_container.login_container.welcome_msg)    
            self.sidebar_container.login_container.box3.add_widget(self.sidebar_container.login_container.staff_portal)
            self.sidebar_container.login_container.box4.add_widget(self.sidebar_container.login_container.logout_submit)
            self.sidebar_container.cart_container.box3.add_widget(self.sidebar_container.cart_container.staff_message)
            self.sidebar_container.login_container.welcome_msg.text = f'Welcome, {sql.get_name(gui_globals.u_id)[0]}'
        elif gui_globals.c_id and not gui_globals.s_id:
            self.sidebar_container.login_container.box1.clear_widgets()
            self.sidebar_container.login_container.box2.clear_widgets()            
            self.sidebar_container.login_container.box3.clear_widgets()
            self.sidebar_container.login_container.box4.clear_widgets()
            self.sidebar_container.login_container.box5.clear_widgets()
            self.sidebar_container.cart_container.box3.clear_widgets()
            self.sidebar_container.cart_container.box4.clear_widgets()
            self.sidebar_container.cart_container.box5.clear_widgets()    
            self.sidebar_container.login_container.box1.add_widget(self.sidebar_container.login_container.welcome_msg)
            self.sidebar_container.login_container.box3.add_widget(self.sidebar_container.login_container.account_info)
            self.sidebar_container.login_container.box4.add_widget(self.sidebar_container.login_container.logout_submit)
            self.sidebar_container.cart_container.box3.add_widget(self.sidebar_container.cart_container.cart_items)
            self.sidebar_container.cart_container.box4.add_widget(self.sidebar_container.cart_container.cart_balance)
            self.sidebar_container.cart_container.box5.add_widget(self.sidebar_container.cart_container.view_cart_btn)
            self.display_products_page(0)
            self.sidebar_container.login_container.welcome_msg.text = f'Welcome, {sql.get_name(gui_globals.u_id)[0]}'
            self.sidebar_container.cart_container.cart_items.text = f'Items: {sql.get_num_items(gui_globals.cart_id)}'
            self.sidebar_container.cart_container.cart_balance.text = f'Balance: ${sql.get_cart_total(gui_globals.cart_id)}'

    def init_sidebar_logout(self):
        self.sidebar_container.login_container.box1.clear_widgets()
        self.sidebar_container.login_container.box2.clear_widgets()
        self.sidebar_container.login_container.box3.clear_widgets()
        self.sidebar_container.login_container.box3.clear_widgets()
        self.sidebar_container.login_container.box4.clear_widgets()
        self.sidebar_container.login_container.box5.clear_widgets()
        self.sidebar_container.cart_container.box3.clear_widgets()
        self.sidebar_container.cart_container.box4.clear_widgets()
        self.sidebar_container.cart_container.box5.clear_widgets()
        self.sidebar_container.login_container.box1.add_widget(self.sidebar_container.login_container.ip_username)
        self.sidebar_container.login_container.box2.add_widget(self.sidebar_container.login_container.ip_passwd)
        self.sidebar_container.login_container.box4.add_widget(self.sidebar_container.login_container.login_submit)
        self.sidebar_container.cart_container.box3.add_widget(self.sidebar_container.cart_container.cart_login_err)
        self.workspace_container.clear_widgets()
        self.workspace_container.add_widget(self.workspace_container.products)
        self.display_products_page(0)
        gui_globals.curr_page = 'products'
        self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown_btn.text = "--Select shipping--"
        self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown_btn.text = "--Select payment--"
        self.workspace_container.customer_account.edit_info_form.ip_info_street.text = "street name"
        self.workspace_container.customer_account.edit_info_form.ip_info_apt.text = "apt number"
        self.workspace_container.customer_account.edit_info_form.ip_info_city.text = "city"
        self.workspace_container.customer_account.edit_info_form.ip_info_state.text = "state"
        self.workspace_container.customer_account.edit_info_form.ip_info_zip.text = "zip"
        self.workspace_container.customer_account.edit_info_form.ip_info_card.text = "card number"

    def search_submit(self,instance):
        print(f'search request:: attempting search >> "{self.sidebar_container.searchbar_container.search_bar.text}"')
        gui_globals.search = self.sidebar_container.searchbar_container.search_bar.text
        gui_globals.filt_products = sql.search_products(gui_globals.search, gui_globals.cat_id, gui_globals.cart_id)
        gui_globals.search = ""
        self.sidebar_container.searchbar_container.search_bar.text = ""

    def associate_cart(self,cust_id):
        if not cust_id:
            return
        cart = sql.find_open_cart(cust_id)
        if not cart:
            cart = sql.add_cart(cust_id)
        return cart

    def add_to_cart(self,instance):
        quant = self.workspace_container.products.product_boxes[instance.btn_id].ip_product_add.text
        if not quant.isdigit():
            return
        else:
            quant = int(quant)
        if quant < 1 or quant > sql.quantity_in_stock(instance.prod_id,gui_globals.cart_id):
            return
        sql.add_to_cart(gui_globals.cart_id, instance.prod_id, quant)
        self.refresh_cart()

    def remove_from_cart(self,instance):
        quant = self.workspace_container.cart.cart_boxes[instance.btn_id].ip_product_add.text
        if not quant.isdigit():
            return
        else:
            quant = int(quant)
        if quant < 0 or quant > sql.quantity_in_stock(instance.prod_id,gui_globals.cart_id):
            return
        sql.modify_cart(gui_globals.cart_id, instance.prod_id, quant)
        self.workspace_container.cart.cart_boxes[instance.btn_id].product_stock.text = f'Quantity: {quant}'
        self.refresh_cart()

    def set_shipping_option(self,instance):
        sql.set_cart_addr(gui_globals.cart_id, instance.ship_id)
        self.display_cart_page(0)

    def set_payment_option(self,instance):
        pass

    def submit_order(self,instance):
        sql.submit_order(gui_globals.cart_id, int(self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown_btn.text))
        gui_globals.cart_id = sql.add_cart(gui_globals.c_id)
        self.refresh_cart()
        self.display_cart_page(0)

    def refresh_cart(self):
        self.sidebar_container.cart_container.cart_items.text = f'Items: {sql.get_num_items(gui_globals.cart_id)}'
        self.sidebar_container.cart_container.cart_balance.text = f'Balance: ${sql.get_cart_total(gui_globals.cart_id)}'

    def update_customer_fields(self,instance):
        if instance.btn_id == 'payment':
            gui_globals.card_id = instance.card_id
            gui_globals.shipping_or_payment = instance.btn_id
            from_card = sql.get_payment(gui_globals.c_id, int(instance.text))
            if not from_card:
                return
            self.workspace_container.customer_account.edit_info_form.ip_info_street.text = from_card[1]
            self.workspace_container.customer_account.edit_info_form.ip_info_apt.text = from_card[2]
            self.workspace_container.customer_account.edit_info_form.ip_info_city.text = from_card[3]
            self.workspace_container.customer_account.edit_info_form.ip_info_state.text = from_card[4]
            self.workspace_container.customer_account.edit_info_form.ip_info_zip.text = from_card[5]
            self.workspace_container.customer_account.edit_info_form.ip_info_card.text = from_card[0]
        elif instance.btn_id == 'shipping':
            gui_globals.ship_id = instance.ship_id
            gui_globals.shipping_or_payment = instance.btn_id
            from_addr = sql.get_address(instance.ship_id)
            if not from_addr:
                return
            self.workspace_container.customer_account.edit_info_form.ip_info_street.text = from_addr[1]
            self.workspace_container.customer_account.edit_info_form.ip_info_apt.text = from_addr[2]
            self.workspace_container.customer_account.edit_info_form.ip_info_city.text = from_addr[3]
            self.workspace_container.customer_account.edit_info_form.ip_info_state.text = from_addr[4]
            self.workspace_container.customer_account.edit_info_form.ip_info_zip.text = from_addr[5]
            self.workspace_container.customer_account.edit_info_form.ip_info_card.text = "N/A"

    def update_customer_info(self,instance):
        if gui_globals.shipping_or_payment == 'shipping':
            sql.modify_ship_addr(gui_globals.ship_id, self.workspace_container.customer_account.edit_info_form.ip_info_street.text,
                                                        self.workspace_container.customer_account.edit_info_form.ip_info_city.text,
                                                        self.workspace_container.customer_account.edit_info_form.ip_info_state.text,
                                                        self.workspace_container.customer_account.edit_info_form.ip_info_zip.text,
                                                        self.workspace_container.customer_account.edit_info_form.ip_info_apt.text )
            self.display_my_account_page(0)
        elif gui_globals.shipping_or_payment == 'payment':
            sql.modify_payment(gui_globals.c_id, gui_globals.card_id, self.workspace_container.customer_account.edit_info_form.ip_info_street.text,
                                                        self.workspace_container.customer_account.edit_info_form.ip_info_card.text,
                                                        self.workspace_container.customer_account.edit_info_form.ip_info_apt.text,
                                                        self.workspace_container.customer_account.edit_info_form.ip_info_city.text,
                                                        self.workspace_container.customer_account.edit_info_form.ip_info_state.text,
                                                        self.workspace_container.customer_account.edit_info_form.ip_info_zip.text )
            self.display_my_account_page(0)

    def remove_customer_info(self,instance):
        if gui_globals.shipping_or_payment == 'shipping':
            if gui_globals.ship_id == sql.get_cart_addr(gui_globals.cart_id):
                return
            sql.del_ship_addr(gui_globals.ship_id, gui_globals.c_id)
            self.display_my_account_page(0)
        elif gui_globals.shipping_or_payment == 'payment':
            sql.del_payment(gui_globals.c_id, gui_globals.card_id)
            self.display_my_account_page(0)

    def select_product(self,instance):
        gui_globals.prod_id = instance.prod_id
        self.workspace_container.staff_controls.selected_product.text = f'Product: {instance.prod_name} ({instance.prod_id})'
        self.display_staff_portal_page(0)

    def price_update(self,instance):
        state = self.workspace_container.staff_controls.ip_state_update.text
        state.upper()
        price = self.workspace_container.staff_controls.ip_price_update.text
        if not state or not price or not gui_globals.prod_id:
            return
        if state not in gui_globals.state_list:
            self.workspace_container.staff_controls.ip_state_update.text = 'inval'
            return
        if not self.isnumber(price):
            self.workspace_container.staff_controls.ip_price_update.text = 'inval'
            return
        elif float(price) < 0:
            self.workspace_container.staff_controls.ip_price_update.text = 'inval'
            return
        sql.modify_price(gui_globals.prod_id,state,float(price))
        pass

    def stock_update(self,instance):
        if not gui_globals.supp_id or not gui_globals.wh_id:
            return
        stock = self.workspace_container.staff_controls.ip_stock_update.text
        if not stock.isdigit() or int(stock) < 0:
            return
        sql.modify_stock(gui_globals.prod_id, gui_globals.wh_id, "+" + str(stock))

    def set_supplier(self,instance):
        gui_globals.supp_id = instance.supp_id

    def set_warehouse(self,instance):
        gui_globals.wh_id = instance.wh_id

    def isnumber(self,val):
        try:
            float(val)
            return True
        except ValueError:
            return False
        
    def display_products_page(self,instance):
        
        if gui_globals.curr_page != 'products':
            gui_globals.curr_page = 'products'
            self.workspace_container.clear_widgets()
            self.workspace_container.add_widget(self.workspace_container.products)
        
        # clear page contents
        for i in range(10):
            if i + gui_globals.product_page_indexer > gui_globals.product_num_products:
                break
            self.workspace_container.products.remove_widget(self.workspace_container.products.lines[i])
            self.workspace_container.products.lines[i].clear_widgets()
            self.workspace_container.products.product_boxes[i].clear_widgets()

        # update control params
        gui_globals.product_num_products = len(gui_globals.filt_products) - 1
        if gui_globals.product_num_products < 0:
            gui_globals.product_num_products = 0
        gui_globals.product_num_pages = gui_globals.product_num_products // 10 + 1

        # set goto page number
        temp_goto_page = self.workspace_container.products.page_goto.ip_goto_page.text
        if temp_goto_page.isdigit():
            if int(temp_goto_page) > 0 and int(temp_goto_page) <= gui_globals.product_num_pages:
                goto_page = int(temp_goto_page)
            elif int(temp_goto_page) > gui_globals.product_num_pages:
                goto_page = gui_globals.product_num_pages
            else:
                goto_page = 1
        else:
            goto_page = 1

        #setup page indexer and update text fields
        gui_globals.product_page_indexer = 10 * (goto_page - 1)

        self.workspace_container.products.page_goto.ip_goto_page.text = str(goto_page)
        self.workspace_container.products.page_goto.goto_msg.text=f' / {gui_globals.product_num_pages} pages'        

        # clear lines and product_boxes lists
        self.workspace_container.products.lines.clear()
        self.workspace_container.products.product_boxes.clear()

        # update page contents
        for i in range(10):
            if i + gui_globals.product_page_indexer > gui_globals.product_num_products:
                break
            self.workspace_container.products.lines.append(generic_box(workspace_size_x-8,default_line_height+10))
            self.workspace_container.products.lines[i].padding = (125,0)
            self.workspace_container.products.add_widget(self.workspace_container.products.lines[i])
            self.workspace_container.products.product_boxes.append(product_box())
            self.workspace_container.products.lines[i].add_widget(self.workspace_container.products.product_boxes[i])
            if gui_globals.product_num_products > 0:
                if gui_globals.c_id:
                    self.workspace_container.products.product_boxes[i].product_title.text= gui_globals.filt_products[i + gui_globals.product_page_indexer][1]
                    self.workspace_container.products.product_boxes[i].product_price.text= f'Price: ${gui_globals.filt_products[i + gui_globals.product_page_indexer][4]}'
                    self.workspace_container.products.product_boxes[i].product_stock.text= f'Stock: {sql.quantity_in_stock(gui_globals.filt_products[i + gui_globals.product_page_indexer][0], gui_globals.cart_id)}'
                    self.workspace_container.products.product_boxes[i].cart_add.prod_id = gui_globals.filt_products[i + gui_globals.product_page_indexer][0]
                    self.workspace_container.products.product_boxes[i].cart_add.btn_id = i
                    self.workspace_container.products.product_boxes[i].cart_add.bind(on_release=self.add_to_cart)
                else:
                    self.workspace_container.products.product_boxes[i].product_title.text= gui_globals.filt_products[i + gui_globals.product_page_indexer][1]
                    self.workspace_container.products.product_boxes[i].product_price.text= f'Login as Customer to see prices/stock'
                    self.workspace_container.products.product_boxes[i].product_stock.text= f''
            else:
                self.workspace_container.products.product_boxes[i].product_title.text= f'No items to show'
                self.workspace_container.products.product_boxes[i].product_price.text= f''
                self.workspace_container.products.product_boxes[i].product_stock.text= f''

    def display_my_account_page(self,instance):

        if gui_globals.curr_page != 'customer':
            gui_globals.curr_page = 'customer'
            self.workspace_container.clear_widgets()
            self.workspace_container.add_widget(self.workspace_container.customer_account)

        self.workspace_container.customer_account.boxes[11].clear_widgets()
        self.workspace_container.customer_account.boxes[15].clear_widgets()

        self.workspace_container.customer_account.cust_first_name.text = f'Name:             {sql.get_name(gui_globals.u_id)[0]}'
        self.workspace_container.customer_account.cust_last_name.text = f'Surname:          {sql.get_name(gui_globals.u_id)[1]}'
        self.workspace_container.customer_account.cust_id.text = f'Customer ID:     {str(gui_globals.c_id)}'
        self.workspace_container.customer_account.cust_outstanding_balance.text = f'Outstanding Balance: {sql.get_balance(gui_globals.c_id)[0]}'

        for address in gui_globals.curr_shipping_list:
            self.workspace_container.customer_account.cust_shipping_dropdown.clear_widgets()

        for card in gui_globals.curr_card_list:
            self.workspace_container.customer_account.cust_payment_dropdown.clear_widgets()

        gui_globals.curr_card_list = sql.get_payments(gui_globals.c_id)
        gui_globals.curr_shipping_list = sql.get_addresses(gui_globals.c_id)

        for address in gui_globals.curr_shipping_list:
            cust_shipping_option = Button(text=address[1],size_hint_y=None,height=default_line_height,font_size=11)
            cust_shipping_option.btn_id = 'shipping' 
            cust_shipping_option.ship_id = address[0]
            cust_shipping_option.bind(on_release=lambda cust_shipping_option: self.workspace_container.customer_account.cust_shipping_dropdown.select(cust_shipping_option.text))
            cust_shipping_option.bind(on_release=self.update_customer_fields)
            self.workspace_container.customer_account.cust_shipping_dropdown.add_widget(cust_shipping_option)
        self.workspace_container.customer_account.cust_shipping_dropdown_btn.bind(on_release=self.workspace_container.customer_account.cust_shipping_dropdown.open)
        self.workspace_container.customer_account.boxes[11].add_widget(self.workspace_container.customer_account.cust_shipping_dropdown_btn)

        for card in gui_globals.curr_card_list:
            cust_payment_option = Button(text=card[0],size_hint_y=None,height=default_line_height,font_size=11)
            cust_payment_option.btn_id = 'payment'
            cust_payment_option.card_id = card[0]
            cust_payment_option.bind(on_release=lambda cust_payment_option: self.workspace_container.customer_account.cust_payment_dropdown.select(cust_payment_option.text))
            cust_payment_option.bind(on_release=self.update_customer_fields)
            self.workspace_container.customer_account.cust_payment_dropdown.add_widget(cust_payment_option)
        self.workspace_container.customer_account.cust_payment_dropdown_btn.bind(on_release=self.workspace_container.customer_account.cust_payment_dropdown.open)
        self.workspace_container.customer_account.boxes[15].add_widget(self.workspace_container.customer_account.cust_payment_dropdown_btn)

        self.workspace_container.customer_account.edit_info_form.info_submit.bind(on_release=self.update_customer_info)
        self.workspace_container.customer_account.edit_info_form.info_remove.bind(on_release=self.remove_customer_info)

    def display_staff_portal_page(self,instance):

        if gui_globals.curr_page != 'staff':
            gui_globals.curr_page = 'staff'
            self.workspace_container.clear_widgets()
            self.workspace_container.add_widget(self.workspace_container.staff_controls)

        # update control params
        gui_globals.all_products = sql.search_products(None,None,None)
        gui_globals.staff_num_products = len(gui_globals.all_products) - 1
        if gui_globals.staff_num_products < 0:
            gui_globals.staff_num_products = 0
        gui_globals.staff_num_pages = gui_globals.staff_num_products // 5 + 1

        suppliers = sql.get_suppliers()
        warehouses = sql.get_warehouses()

        print(warehouses)

        for s in suppliers:
            self.workspace_container.staff_controls.stock_supplier_dropdown.clear_widgets()

        for wh in warehouses:
            self.workspace_container.staff_controls.stock_warehouse_dropdown.clear_widgets()

        self.workspace_container.staff_controls.stock_box.remove_widget(self.workspace_container.staff_controls.stock_supplier_dropdown_btn)
        self.workspace_container.staff_controls.stock_box.remove_widget(self.workspace_container.staff_controls.stock_warehouse_dropdown_btn)

        for s in suppliers:
            btn = Button(text=str(s[1]),size_hint_y=None,height=default_line_height,font_size=11)
            btn.supp_id = s[0]
            btn.bind(on_release=lambda btn: self.workspace_container.staff_controls.stock_supplier_dropdown.select(btn.text))
            btn.bind(on_release=self.set_supplier)
            self.workspace_container.staff_controls.stock_supplier_dropdown.add_widget(btn)
        self.workspace_container.staff_controls.stock_supplier_dropdown_btn.bind(on_release=self.workspace_container.staff_controls.stock_supplier_dropdown.open)
        self.workspace_container.staff_controls.stock_box.add_widget(self.workspace_container.staff_controls.stock_supplier_dropdown_btn)
        self.workspace_container.staff_controls.stock_supplier_dropdown.bind(on_select=lambda instance, x: setattr(self.workspace_container.staff_controls.stock_supplier_dropdown_btn, 'text', x))

        for wh in warehouses:
            btn = Button(text=str(wh[1]),size_hint_y=None,height=default_line_height,font_size=11)
            btn.wh_id = wh[0]
            btn.bind(on_release=lambda btn: self.workspace_container.staff_controls.stock_warehouse_dropdown.select(btn.text))
            btn.bind(on_release=self.set_warehouse)
            self.workspace_container.staff_controls.stock_warehouse_dropdown.add_widget(btn)
        self.workspace_container.staff_controls.stock_warehouse_dropdown_btn.bind(on_release=self.workspace_container.staff_controls.stock_warehouse_dropdown.open)
        self.workspace_container.staff_controls.stock_box.add_widget(self.workspace_container.staff_controls.stock_warehouse_dropdown_btn)
        self.workspace_container.staff_controls.stock_warehouse_dropdown.bind(on_select=lambda instance, x: setattr(self.workspace_container.staff_controls.stock_warehouse_dropdown_btn, 'text', x))


        # set goto page number
        temp_goto_page = self.workspace_container.staff_controls.page_goto.ip_goto_page.text
        if temp_goto_page.isdigit():
            if int(temp_goto_page) > 0 and int(temp_goto_page) <= gui_globals.staff_num_pages:
                goto_page = int(temp_goto_page)
            elif int(temp_goto_page) > gui_globals.staff_num_pages:
                goto_page = gui_globals.staff_num_pages
            else:
                goto_page = 1
        else:
            goto_page = 1

        #setup page indexer and update text fields
        self.workspace_container.staff_controls.page_goto.ip_goto_page.text = str(goto_page)
        self.workspace_container.staff_controls.page_goto.goto_msg.text=f' / {gui_globals.staff_num_pages} pages'
        gui_globals.staff_page_indexer = 5 * (goto_page - 1)

        for i in range(5):
            if i + gui_globals.staff_page_indexer > gui_globals.staff_num_products:
                break
            self.workspace_container.staff_controls.staff_product_boxes[i].product_title.text=f'{gui_globals.all_products[i + gui_globals.staff_page_indexer][1]} ({gui_globals.all_products[i + gui_globals.staff_page_indexer][0]})'
            self.workspace_container.staff_controls.staff_product_boxes[i].product_select.prod_id = gui_globals.all_products[i + gui_globals.staff_page_indexer][0]
            self.workspace_container.staff_controls.staff_product_boxes[i].product_select.prod_name = gui_globals.all_products[i + gui_globals.staff_page_indexer][1]
            self.workspace_container.staff_controls.staff_product_boxes[i].product_select.bind(on_release=self.select_product)


    def display_cart_page(self,instance):

        if gui_globals.curr_page != 'cart':
            gui_globals.curr_page = 'cart'
            self.workspace_container.clear_widgets()
            self.workspace_container.add_widget(self.workspace_container.cart)

        # clear page contents
        if gui_globals.curr_cart_size <= 0:
            self.workspace_container.cart.remove_widget(self.workspace_container.cart.lines[0])
            self.workspace_container.cart.lines[0].clear_widgets()
            self.workspace_container.cart.cart_boxes[0].clear_widgets()
        for i in range(10):
            if i + gui_globals.cart_page_indexer > gui_globals.curr_cart_size:
                break
            self.workspace_container.cart.remove_widget(self.workspace_container.cart.lines[i])
            self.workspace_container.cart.lines[i].clear_widgets()
            self.workspace_container.cart.cart_boxes[i].clear_widgets()

        gui_globals.curr_cart = sql.list_cart(gui_globals.cart_id)
        gui_globals.curr_cart_size = len(gui_globals.curr_cart) - 1
        gui_globals.cart_num_pages = gui_globals.curr_cart_size // 10 + 1
        if gui_globals.cart_num_pages <= 0:
            gui_globals.cart_num_pages = 1

        for address in gui_globals.curr_shipping_list:
            self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown.clear_widgets()

        for card in gui_globals.curr_card_list:
            self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown.clear_widgets()

        self.workspace_container.cart.cart_checkout_line.box.clear_widgets()

        gui_globals.curr_card_list = sql.get_payments(gui_globals.c_id)
        gui_globals.curr_shipping_list = sql.get_addresses(gui_globals.c_id)

        for address in gui_globals.curr_shipping_list:
            shipping_option = Button(text=address[1],size_hint_y=None,height=default_line_height,font_size=11)
            shipping_option.ship_id = address[0]
            shipping_option.bind(on_release=lambda shipping_option: self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown.select(shipping_option.text))
            shipping_option.bind(on_release=self.set_shipping_option)
            self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown.add_widget(shipping_option)
        self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown_btn.bind(on_release=self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown.open)
        self.workspace_container.cart.cart_checkout_line.box.add_widget(self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown_btn)
        self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown.bind(on_select=lambda instance, x: setattr(self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown_btn, 'text', x))
        self.workspace_container.cart.cart_checkout_line.cart_shipping_dropdown_btn.text = sql.get_address(sql.get_cart_addr(gui_globals.cart_id))[1]

        for card in gui_globals.curr_card_list:
            payment_option = Button(text=card[0],size_hint_y=None,height=default_line_height,font_size=11)
            payment_option.bind(on_release=lambda payment_option: self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown.select(payment_option.text))
            payment_option.bind(on_release=self.set_payment_option)
            self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown.add_widget(payment_option)
        self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown_btn.bind(on_release=self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown.open)
        self.workspace_container.cart.cart_checkout_line.box.add_widget(self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown_btn)
        self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown.bind(on_select=lambda instance, x: setattr(self.workspace_container.cart.cart_checkout_line.cart_credit_card_dropdown_btn, 'text', x))

        self.workspace_container.cart.cart_checkout_line.box.add_widget(self.workspace_container.cart.cart_checkout_line.cart_submit_btn)
        self.workspace_container.cart.cart_checkout_line.cart_submit_btn.bind(on_release=self.submit_order)

        # set goto page number
        temp_goto_page = self.workspace_container.cart.page_goto.ip_goto_page.text
        if temp_goto_page.isdigit():
            if int(temp_goto_page) > 0 and int(temp_goto_page) <= gui_globals.cart_num_pages:
                goto_page = int(temp_goto_page)
            elif int(temp_goto_page) > gui_globals.cart_num_pages:
                goto_page = gui_globals.cart_num_pages
            else:
                goto_page = 1
        else:
            goto_page = 1

        #setup page indexer and update text fields
        gui_globals.cart_page_indexer = 10 * (goto_page - 1)

        self.workspace_container.cart.page_goto.ip_goto_page.text = str(goto_page)
        self.workspace_container.cart.page_goto.goto_msg.text=f' / {gui_globals.cart_num_pages} pages'        

        # clear lines and product_boxes lists
        self.workspace_container.cart.lines.clear()
        self.workspace_container.cart.cart_boxes.clear()

        if gui_globals.curr_cart_size >= 0:
            for i in range(10):
                if i + gui_globals.cart_page_indexer > gui_globals.curr_cart_size:
                    break
                self.workspace_container.cart.lines.append(generic_box(workspace_size_x-8,default_line_height+10))
                self.workspace_container.cart.lines[i].padding = (125,0)
                self.workspace_container.cart.add_widget(self.workspace_container.cart.lines[i])
                self.workspace_container.cart.cart_boxes.append(cart_box())
                self.workspace_container.cart.lines[i].add_widget(self.workspace_container.cart.cart_boxes[i])
                self.workspace_container.cart.cart_boxes[i].product_title.text = sql.get_product(gui_globals.curr_cart[i][0])[1]
                self.workspace_container.cart.cart_boxes[i].product_price.text = f'Price: {gui_globals.curr_cart[i][3]}'
                self.workspace_container.cart.cart_boxes[i].product_stock.text = f'Quantity: {gui_globals.curr_cart[i][2]}'
                self.workspace_container.cart.cart_boxes[i].cart_add.prod_id = gui_globals.filt_products[i + gui_globals.product_page_indexer][0]
                self.workspace_container.cart.cart_boxes[i].cart_add.btn_id = i
                self.workspace_container.cart.cart_boxes[i].cart_add.bind(on_release=self.remove_from_cart)
        else:
            self.workspace_container.cart.lines.append(generic_box(workspace_size_x-8,default_line_height+10))
            self.workspace_container.cart.lines[0].padding = (125,0)
            self.workspace_container.cart.add_widget(self.workspace_container.cart.lines[0])
            self.workspace_container.cart.cart_boxes.append(cart_box())
            self.workspace_container.cart.lines[0].add_widget(self.workspace_container.cart.cart_boxes[0])
            self.workspace_container.cart.cart_boxes[0].product_title.text = ''
            self.workspace_container.cart.cart_boxes[0].product_price.text = 'No Items in cart'
            self.workspace_container.cart.cart_boxes[0].product_stock.text = ''

class storegui(App):
    def build(self):
        return landing()

if __name__ == "__main__":
    storegui().run()
