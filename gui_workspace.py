
import os
import sql
import kivy
import gui_globals
from gui_pages import *
from kivy.uix.gridlayout import GridLayout
from kivy.graphics import *
kivy.require("1.11.1")


workspace_size_x = 550
sidebar_size_x = 160
page_size_y = 200
loginbox_size_y = 80
userbox_size_y = 80
cartbox_size_y = 110
filterbox_size_y = 35
searchbar_size_y = 35
default_line_height = 25


class workspace(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.cols = 1
        self.rows = 1
        self.size_hint = (None,None)
        self.size = (workspace_size_x,520)
        self.padding = 5

        self.products = page_products()
        self.cart = page_cart_overview()
        self.customer_account = page_customer_account()
        self.staff_controls = page_staff_control()
        self.add_widget(self.products)
        
        with self.canvas.before:
            Color(1,1,1,1)
            self.rect = Rectangle(size=self.size, pos=self.pos)
            self.bind(pos=self.update_rect, size=self.update_rect)

    def update_rect(self,*args):
        self.rect.pos = self.pos
        self.rect.size = self.size


        
        
