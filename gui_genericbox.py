
import os
import sql
import kivy
import gui_globals
from kivy.uix.gridlayout import GridLayout
kivy.require("1.11.1")

class generic_box(GridLayout):
    def __init__(self,h,w,**kwargs):
        super().__init__(**kwargs)

        self.rows = 1
        self.cols = 1

        self.size_hint = (None,None)
        self.size = (h,w)
        
