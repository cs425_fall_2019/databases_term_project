
import os
import sql
import kivy
import gui_globals
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.dropdown import DropDown
from kivy.graphics import *
kivy.require("1.11.1")

sidebar_size_x = 160
loginbox_size_y = 140
userbox_size_y = 80
cartbox_size_y = 110
filterbox_size_y = 35
searchbar_size_y = 35
default_line_height = 25

class filterbox(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.rows = 1
        self.cols = 1
        self.size_hint = (None, None)
        self.size = (160,35)
        self.padding = 4

        self.filterbox_dropdown = DropDown()

        categories = sql.get_categories()

        # add none selection
        btn = Button(text="None", size_hint_y=None, height=27, font_size=11)
        btn.bind(on_release=lambda btn: self.filterbox_dropdown.select(btn.text))
        btn.bind(on_press=lambda btn: self.set_cat_id(None))
        self.filterbox_dropdown.add_widget(btn)

        for category in categories:
            btn = Button(text=category[1], size_hint_y=None, height=27, font_size=11)
            btn.categ_id = category[0]
            btn.bind(on_release=lambda btn: self.filterbox_dropdown.select(btn.text))
            btn.bind(on_press=lambda btn: self.set_cat_id(btn.categ_id))
            self.filterbox_dropdown.add_widget(btn)

        filterbox_btn = Button(text="Filter by", size=(152, 27), font_size=11)
        filterbox_btn.bind(on_release=self.filterbox_dropdown.open)
        self.add_widget(filterbox_btn)
        self.filterbox_dropdown.bind(on_select=lambda instance, x: setattr(filterbox_btn, 'text', x))

        with self.canvas.before:
            Color(1,1,1,1)
            self.rect = Rectangle(size=self.size, pos=self.pos)
            self.bind(pos=self.update_rect, size=self.update_rect)

    def update_rect(self,*args):
        self.rect.pos = self.pos
        self.rect.size = self.size

    def set_cat_id(self,categ_id):
        gui_globals.cat_id = categ_id
        gui_globals.filt_products = sql.search_products(gui_globals.search, gui_globals.cat_id, gui_globals.cart_id)
