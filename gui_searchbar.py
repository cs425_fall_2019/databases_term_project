
import os
import sql
import kivy
import gui_globals
from gui_separator import separator
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.graphics import *
kivy.require("1.11.1")

sidebar_size_x = 160
loginbox_size_y = 140
userbox_size_y = 80
cartbox_size_y = 110
filterbox_size_y = 35
searchbar_size_y = 35
default_line_height = 25

class searchbar(GridLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        self.rows = 1
        self.cols = 3
        self.size_hint = (None,None)
        self.size = (sidebar_size_x,searchbar_size_y)
        self.padding = 4

        self.search_bar = TextInput(hint_text="Search products",
                                    multiline=False,
                                    size_hint = (None,1),
                                    width = 125,
                                    font_size=10)
        self.search_submit = Button(text="Go",
                                    size_hint=(None,1),
                                    width=10,
                                    font_size=10)

        self.add_widget(self.search_bar)
        self.add_widget(separator(None,10,1,1,1,1))
        self.add_widget(self.search_submit)
        
        with self.canvas.before:
            Color(1,1,1,1)
            self.rect = Rectangle(size=self.size, pos=self.pos)
            self.bind(pos=self.update_rect, size=self.update_rect)

    def update_rect(self,*args):
        self.rect.pos = self.pos
        self.rect.size = self.size
        
